#!/usr/bin/env python

from setuptools import setup, find_packages


def install_requires():
  with open("requirements-compute.txt") as f:
    for line in f:
      yield line.strip()

  with open("requirements-server.txt") as f:
    for line in f:
      yield line.strip()


setup(
  name="cfdbox",
  version="0.1",
  description="cfdbox general",
  author="Shuhao Wu",
  author_email="shuhao@shuhaowu.com",
  url="https://gitlab.com/shuhao/cfdbox",
  packages=list(find_packages()),
  include_package_data=True,
  scripts=["bin/cfdbox-compute", "bin/cs-cfx"]
)
