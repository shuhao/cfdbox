CFDBox
======

A collection of tools for helping running and processing CFD simulations.

Server setup
------------

For server setup, see the `server` and `provisioning` directory. `server` has
all the raw tools. You must manually set it up on the server if only use the tools available in that directory. `provisioning` allows you to setup a server from your computer with 1 command.

Components
----------

### server ###

This directory has all the server tools. Right now it's just CFX. This directory also has the documentation on how the server will be setup.

### scripts ###

Some shortcut scripts.

### provisioning ###

Using ansible to setup a server automatically. One command to setup a server.
