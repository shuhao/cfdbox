from __future__ import absolute_import

from cfdbox.compute.core.workunit import WorkUnit


class UnitTestWorkUnit(WorkUnit):
  description = "unit test work unit"

  @classmethod
  def initialize(cls, *args, **kwargs):
    super(UnitTestWorkUnit, cls).initialize(*args, **kwargs)
    cls.parser.add_argument("inputs", nargs="+")

  @classmethod
  def split_input_from_args(cls, args):
    input_config = vars(args)
    local_data = []
    for i in args.inputs:
      local_data.append({"data": i})

    return input_config, local_data

  @classmethod
  def validate_inputs(cls, global_data, local_data):
    for item in local_data:
      if item["data"] == "fail_me":
        return False, "failed reason"
    return True, ""

  def id(self):
    return self._local_data["data"]

  def work(self):
    """Runs and returns whether or not the run is successful"""
    self._output = "11{}11".format(self._local_data["data"])
    self._done()


class UnitTestWorkUnitRunner(object):
  current_runner = None

  def __init__(self, config):
    self.config = config
    self.__class__.current_runner = self

  def run(self, workunit_cls, global_data, local_data):
    def _run_one(data):
      unit = workunit_cls(global_data, data)
      unit.work()
      return unit.id(), unit.output()

    return map(_run_one, local_data)
