from __future__ import absolute_import

import unittest

from ..helpers import UnitTestWorkUnitRunner, UnitTestWorkUnit


class TestBasicWorkUnit(unittest.TestCase):
  def setUp(self):
    UnitTestWorkUnitRunner.current_runner = None

  def test_work_unit_main(self):
    outputs = dict(UnitTestWorkUnit.main(runner_cls=UnitTestWorkUnitRunner, args=["--parallel", "3", "abc", "cba", "ccc"]))

    self.assertEqual(3, UnitTestWorkUnitRunner.current_runner.config["parallel"])
    self.assertEqual(3, len(outputs))
    expected_outputs = {"abc": "11abc11", "cba": "11cba11", "ccc": "11ccc11"}
    self.assertEqual(expected_outputs, outputs)

  def test_work_unit_main_fail_validation(self):
    with self.assertRaises(SystemExit):
      UnitTestWorkUnit.main(runner_cls=UnitTestWorkUnitRunner, args=["--parallel", "3", "abc", "fail_me", "ccc"])
