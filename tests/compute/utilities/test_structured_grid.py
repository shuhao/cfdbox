from __future__ import absolute_import, division, print_function

import unittest


from cfdbox.compute.utilities.structured_grid import StructuredGrid


class TestStructuredGrid(unittest.TestCase):
  def test_simple_structured_grid(self):
    test_grid = [
      (0, 0, 0),
      (0, 0, 1),
      (0, 0, 2),

      (0, 1, 0),
      (0, 1, 1),
      (0, 1, 2),

      (0, 2, 0),
      (0, 2, 1),
      (0, 2, 2),

      (1, 0, 0),
      (1, 0, 1),
      (1, 0, 2),

      (1, 1, 0),
      (1, 1, 1),
      (1, 1, 2),

      (1, 2, 0),
      (1, 2, 1),
      (1, 2, 2),

      (2, 0, 0),
      (2, 0, 1),
      (2, 0, 2),

      (2, 1, 0),
      (2, 1, 1),
      (2, 1, 2),

      (2, 2, 0),
      (2, 2, 1),
      (2, 2, 2),
    ]

    expected_grid = [
      [0, 0, 0],
      [1, 0, 0],
      [2, 0, 0],

      [0, 1, 0],
      [1, 1, 0],
      [2, 1, 0],

      [0, 2, 0],
      [1, 2, 0],
      [2, 2, 0],

      [0, 0, 1],
      [1, 0, 1],
      [2, 0, 1],

      [0, 1, 1],
      [1, 1, 1],
      [2, 1, 1],

      [0, 2, 1],
      [1, 2, 1],
      [2, 2, 1],

      [0, 0, 2],
      [1, 0, 2],
      [2, 0, 2],

      [0, 1, 2],
      [1, 1, 2],
      [2, 1, 2],

      [0, 2, 2],
      [1, 2, 2],
      [2, 2, 2],
    ]

    structured_grid = StructuredGrid(1e-6)

    # TODO: can't do this unless we can output the seed for debugging
    # Otherwise it is useless.. really
    # random.shuffle(test_grid)
    for line in test_grid:
      structured_grid.append(line[0], line[1], line[2], [])

    structured_grid.finalize()

    actual_grid = list(structured_grid.as_rows())
    self.assertEqual(expected_grid, actual_grid)
    self.assertEqual((3, 3, 3), structured_grid.dimensions())

  def test_rounding_of_number_below_minimum_into_two_numbers(self):
    test_grid = [
      (0, 0, 0),
      (0, 0, 1.85495019e-1),
      (0, 0, 2),

      (0, 1, 0),
      (0, 1, 1.85495019e-1),
      (0, 1, 2),

      (0, 2, 0),
      (0, 2, 1.85495019e-1),
      (0, 2, 2),

      (1, 0, 0),
      (1, 0, 1.85495019e-1),
      (1, 0, 2),

      (1, 1, 0),
      (1, 1, 1.85494989e-1),  # Changed from this point on
      (1, 1, 2),

      (1, 2, 0),
      (1, 2, 1.85494989e-1),
      (1, 2, 2),

      (2, 0, 0),
      (2, 0, 1.85494989e-1),
      (2, 0, 2),

      (2, 1, 0),
      (2, 1, 1.85494989e-1),
      (2, 1, 2),

      (2, 2, 0),
      (2, 2, 1.85494989e-1),
      (2, 2, 2),
    ]

    expected_grid = [
      [0, 0, 0],
      [1, 0, 0],
      [2, 0, 0],

      [0, 1, 0],
      [1, 1, 0],
      [2, 1, 0],

      [0, 2, 0],
      [1, 2, 0],
      [2, 2, 0],

      [0, 0, 1.85495019e-1],
      [1, 0, 1.85495019e-1],
      [2, 0, 1.85494989e-1],

      [0, 1, 1.85495019e-1],
      [1, 1, 1.85494989e-1],
      [2, 1, 1.85494989e-1],

      [0, 2, 1.85495019e-1],
      [1, 2, 1.85494989e-1],
      [2, 2, 1.85494989e-1],

      [0, 0, 2],
      [1, 0, 2],
      [2, 0, 2],

      [0, 1, 2],
      [1, 1, 2],
      [2, 1, 2],

      [0, 2, 2],
      [1, 2, 2],
      [2, 2, 2],
    ]

    structured_grid = StructuredGrid(1e-6)

    for line in test_grid:
      structured_grid.append(line[0], line[1], line[2], [])

    structured_grid.finalize()

    actual_grid = list(structured_grid.as_rows())
    self.assertEqual(expected_grid, actual_grid)
    self.assertEqual((3, 3, 3), structured_grid.dimensions())

  def test_find_closest_coordinate_indexes(self):
    test_grid = [
      (0, 0, 0),
      (0, 1, 0),
      (0, 2, 0),

      (1, 0, 0),
      (1, 1, 0),
      (1, 2, 0),

      (2, 0, 0),
      (2, 1, 0),
      (2, 2, 0),

      (0, 0, 1),
      (0, 1, 1),
      (0, 2, 1),

      (1, 0, 1),
      (1, 1, 1),
      (1, 2, 1),

      (2, 0, 1),
      (2, 1, 1),
      (2, 2, 1),

      (0, 0, 3),
      (0, 1, 3),
      (0, 2, 3),

      (1, 0, 3),
      (1, 1, 3),
      (1, 2, 3),

      (2, 0, 3),
      (2, 1, 3),
      (2, 2, 3),
    ]

    structured_grid = StructuredGrid(1e-6)
    for line in test_grid:
      structured_grid.append(line[0], line[1], line[2], [])

    structured_grid.finalize()

    coordinate_index = structured_grid.find_closest_coordinate_indexes([0.2, 1.2, 1.9], StructuredGrid.DIRECTION_X)
    self.assertEqual([0, 1, 2], coordinate_index)

    coordinate_index = structured_grid.find_closest_coordinate_indexes([0.2, 1.2, 1.9, 2.1], StructuredGrid.DIRECTION_Z)
    self.assertEqual([0, 1, 1, 2], coordinate_index)
