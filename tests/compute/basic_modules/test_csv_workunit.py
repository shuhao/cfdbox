from __future__ import absolute_import

import os
import unittest

from ..helpers import UnitTestWorkUnitRunner

from cfdbox.compute.basic_modules.csv_workunit import CsvWorkUnit


TEST1_CSV_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "..", "testdata", "test1.csv")
TEST2_CSV_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "..", "testdata", "test2.csv")


class UnitTestCsvWorkUnit(CsvWorkUnit):
  description = "unit test csv work unit"

  DEFAULT_LINES_TO_SKIP = 2

  def __init__(self, *args, **kwargs):
    super(UnitTestCsvWorkUnit, self).__init__(*args, **kwargs)
    self._output = []

  def process_line(self, i, line):
    self._output.append((i, line))


class TestBasicWorkUnit(unittest.TestCase):
  def test_process_csv(self):
    outputs = dict(UnitTestCsvWorkUnit.main(UnitTestWorkUnitRunner, ["--csv-file", TEST1_CSV_PATH, TEST2_CSV_PATH]))
    self.assertEqual(2, len(outputs))

    self.assertEqual([(0, ["1", "2", "3", "4"]), (1, ["2", "2", "3", "4"])], outputs[TEST1_CSV_PATH])
    self.assertEqual([(0, ["9", "8", "7", "6"]), (1, ["8", "8", "7", "6"])], outputs[TEST2_CSV_PATH])

  def test_process_csv_failed_with_not_a_file(self):
    with self.assertRaises(SystemExit):
      UnitTestCsvWorkUnit.main(UnitTestWorkUnitRunner, ["--csv-file", TEST1_CSV_PATH, "/lkajdfa/adkfjadf/csv.csv"])
