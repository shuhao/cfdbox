try:
  import unittest
except ImportError:
  import unittest2 as unittest

import os
import shutil
import tempfile

import cfdbox.server.cfx.exporter


class TestExporter(unittest.TestCase):

  CSE_TEMPLATE = """
result_file:{{ result_file }}
result_casename:{{ result_casename }}
export_directory:{{ export_directory }}
trn_filepath:{{ trn_filepath }}
timestep:{{ timestep }}
first:{{ first }}
  """.strip()

  EXPECTED_TRANSIENT_CSE = """
result_file:/tmp/result-abc{timestep}.res
result_casename:result abc{timestep}
export_directory:{export_directory}
trn_filepath:{trn_filepath}
timestep:{timestep}
first:{first}
  """.strip()

  EXPECTED_SINGLE_CSE = """
result_file:/tmp/result-abc.res
result_casename:result abc
export_directory:{export_directory}
trn_filepath:
timestep:
first:
  """.strip()

  def setUp(self):
    self.export_directory = tempfile.mkdtemp()
    self.trn_directory = tempfile.mkdtemp()
    fd, self.cse_template_path = tempfile.mkstemp()
    os.write(fd, self.CSE_TEMPLATE.encode("utf-8"))
    os.close(fd)

    self.export_with_command_and_cse = []

    self.exporter = cfdbox.server.cfx.exporter.Exporter(
      result_file="/tmp/result-abc.res",
      export_directory=self.export_directory,
      cse_template=self.cse_template_path,
      subprocess_call_func=self._subprocess_called
    )

    with open(self.exporter.result_file, "w") as f:
      f.write(" ")

  def tearDown(self):
    shutil.rmtree(self.export_directory, ignore_errors=True)
    shutil.rmtree(self.trn_directory, ignore_errors=True)
    os.remove(self.cse_template_path)
    os.remove(self.exporter.result_file)

  def _subprocess_called(self, cmd):
    # Last option should always be the filename
    with open(cmd[-1]) as f:
      cse = f.read()

    self.export_with_command_and_cse.append([cmd, cse])

  def test_associate_timesteps_with_filenames(self):
    filenames = ["/home/abc/{0}.trn".format(i) for i in range(1000)]
    filenames.append("/home/abc/abc.res")  # this should be ignored
    timesteps = cfdbox.server.cfx.exporter.associate_timesteps_with_filenames(filenames)
    self.assertEqual(1000, len(timesteps))

    for i in range(1000):
      self.assertEqual((i, "/home/abc/{0}.trn".format(i)), timesteps[i])

    timesteps = cfdbox.server.cfx.exporter.associate_timesteps_with_filenames(filenames, filter_with_timesteps=set(range(3, 201)))
    self.assertEqual(198, len(timesteps))

    for i in range(3, 201):
      self.assertEqual((i, "/home/abc/{0}.trn".format(i)), timesteps[i-3])

  def test_exporter_export_transient_file(self):
    trn_path = os.path.join(self.trn_directory, "100.trn")
    self.exporter.export_from_transient_file(100, trn_path, first=True)

    self.assertEqual(1, len(self.export_with_command_and_cse))
    cmd, cse = self.export_with_command_and_cse[-1]
    self.assertEqual(3, len(cmd))
    self.assertEqual("cfx5post", cmd[0])
    self.assertEqual("-batch", cmd[1])
    self.assertEqual(os.path.join(self.export_directory, "export_100.cse"), cmd[2])

    expected_cse = self.EXPECTED_TRANSIENT_CSE.format(
      export_directory=self.export_directory,
      trn_filepath=trn_path,
      timestep=100,
      first=True
    )

    self.assertEqual(expected_cse, cse)
    self.assertFalse(os.path.exists("/tmp/result-abc100.res"))

  def test_exporter_export_result_file(self):
    self.exporter.export_from_result_file()

    self.assertEqual(1, len(self.export_with_command_and_cse))
    cmd, cse = self.export_with_command_and_cse[-1]
    self.assertEqual(3, len(cmd))
    self.assertEqual("cfx5post", cmd[0])
    self.assertEqual("-batch", cmd[1])
    self.assertEqual(os.path.join(self.export_directory, "export_result-abc.cse"), cmd[2])

    expected_cse = self.EXPECTED_SINGLE_CSE.format(
      export_directory=self.export_directory,
    )

    self.assertEqual(expected_cse, cse)
