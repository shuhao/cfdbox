try:
  import unittest
except ImportError:
  import unittest2 as unittest

import cfdbox.server.cfx.utils


class TestCfxUtils(unittest.TestCase):
  def test_get_timestep_from_trn_filename(self):
    timestep = cfdbox.server.cfx.utils.get_timestep_from_trn_filename("/home/abc/trn_files/123.trn")
    self.assertEqual(123, timestep)

    timestep = cfdbox.server.cfx.utils.get_timestep_from_trn_filename("/home/abc/trn_files/123_full.trn")
    self.assertEqual(123, timestep)
