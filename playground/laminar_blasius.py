import csv

from scipy import integrate
import numpy as np

import matplotlib.pyplot as plt


def blasius_derivatives(f, t):
  return [f[1], f[2], -f[0] * f[2] / 2]

eta = np.linspace(0, 24, 3000)
yinit = [0, 0, 0.332057]
feta = integrate.odeint(blasius_derivatives, yinit, eta)

plt.figure()
plt.plot(feta[:, 1], eta)
plt.show()

rows = [[eta[i], feta[:, 1][i]] for i in range(len(eta))]

rows.insert(0, ["eta", "f'(eta)"])

with open("blasius-solution.csv", "w") as f:
  writer = csv.writer(f)
  writer.writerows(rows)
