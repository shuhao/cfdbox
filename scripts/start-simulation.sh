#!/bin/bash

# Change this to your definition filename
DEF_FILE=your-def-file.def

# Possible resume methods are: 
# - cont-from-file: Use initial values and continue the run history from the 
#                   specified ANSYS CFX Results File. See cfx5solve -help for 
#                   more detail (look for cont-from-file).
# - ini-file: Use initial values but discard the run history from the specified
#             ANSYS CFX Results File. See cfx5solve -help for more details 
#             (look for ini-file).
# - none: Do not restart from anything.
RESUME_METHOD=none

# Change this if you are resuming
RESUME_FILE=my-resume-file.res

#################################################################
## DON'T CHANGE THINGS BELOW UNLESS YOU KNOW WHAT YOU'RE DOING ##
#################################################################

if [ -z $TMUX ]; then
  echo "ERROR: you must run this from within a tmux session" >&2
  exit 1
fi

source ~/.cfdboxrc

if [ "$RESUME_METHOD" == "none" ]; then
  cs-cfx run -d $DEF_FILE
else
  cs-cfx run -d $DEF_FILE -m $RESUME_METHOD -c $RESUME_FILE
fi
