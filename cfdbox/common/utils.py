
def is_number(n):
  try:
    float(n)
  except ValueError:
    return False
  else:
    return True
