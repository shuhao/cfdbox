from future.standard_library import install_aliases
install_aliases()

from urllib.request import urlopen, Request
from urllib.error import HTTPError

import base64
import json
import os
import hashlib

# Minimum implementation of Backblaze's B2: https://www.backblaze.com/b2/cloud-storage.html
#
# It is a services that stores some files for you for very low cost.
# As of the time of this writing the first 10GB is free with 1GB download limit
# per day and 2500 actions per day
#
# This is perfect for us for uploading an image and link it in an email.
# This is also better as we can keep a log of things that have happend rather
# than just using email attachments.


class B2(object):

  AUTHORIZE_ACCOUNT_URL = "https://api.backblaze.com/b2api/v1/b2_authorize_account"

  def __init__(self, account_id=None, application_key=None, bucket_id=None):
    self.account_id = account_id or os.environ.get("B2_ACCOUNT_ID")
    self.application_key = application_key or os.environ.get("B2_APPLICATION_KEY")
    self.bucket_id = bucket_id or os.environ.get("B2_BUCKET_ID")
    self.auth_account()

  def auth_account(self):
    encoded_auth = "{0}:{1}".format(self.account_id, self.application_key)
    encoded_auth = base64.b64encode(encoded_auth.encode("utf-8"))
    headers = {
      "Authorization": "Basic {0}".format(encoded_auth.decode("utf-8")),
    }

    request = Request(
      self.AUTHORIZE_ACCOUNT_URL,
      headers=headers,
    )

    response = urlopen(request)
    response_data = json.loads(response.read().decode("utf-8"))
    response.close()

    self.auth_token = response_data["authorizationToken"]
    self.api_url = response_data["apiUrl"]
    self.download_url = response_data["downloadUrl"]

  def make_generic_api_request(self, url, data, headers=None, retried=False):
    if headers is None:
      headers = {}

    headers["Authorization"] = self.auth_token

    request = Request(
      self.url(url),
      data,
      headers=headers
    )

    try:
      response = urlopen(request)
    except HTTPError as e:
      # Reauthenticate if we get a 401 as token will expire.
      if not retried and e.code == 401:
        self.auth_account()
        return self.make_generic_api_request(url, data, headers, retried=True)
      else:
        raise e

    response_data = json.loads(response.read().decode("utf-8"))
    response.close()
    return response_data

  def upload_request(self):
    response_data = self.make_generic_api_request(
      "/b2api/v1/b2_get_upload_url",
      json.dumps({"bucketId": self.bucket_id}).encode("utf-8")
    )
    return response_data["uploadUrl"], response_data["authorizationToken"]

  def upload_file(self, path, filename=None, content_type="b2/x-auto"):
    upload_url, upload_auth_token = self.upload_request()

    with open(path, "rb") as f:
      filedata = f.read()
      sha1data = hashlib.sha1(filedata).hexdigest()

    filesize = os.path.getsize(path)

    headers = {
      "Authorization": upload_auth_token,
      "X-Bz-File-Name": filename or os.path.basename(path),
      "Content-Type": content_type,
      "X-Bz-Content-Sha1": sha1data,
      "Content-Length": filesize
    }

    request = Request(upload_url, filedata, headers=headers)
    response = urlopen(request)
    response_data = json.loads(response.read().decode("utf-8"))
    response.close()

    return self.download_url + "/b2api/v1/b2_download_file_by_id?fileId=" + response_data["fileId"]

  def url(self, url_postfix):
    return "{0}{1}".format(self.api_url, url_postfix)
