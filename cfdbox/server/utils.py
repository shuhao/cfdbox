from __future__ import print_function

import errno
import os
import os.path
import sys


# TODO: change everyone into this scheme please
def validate_file_or_exit(path):
  if not os.path.isfile(path):
    print("ERROR: {0} is not a valid file".format(path), file=sys.stderr)
    sys.exit(1)


def validate_dir_or_exit(path):
  if not os.path.isdir(path):
    print("ERROR: {0} is not a valid directory".format(path), file=sys.stderr)
    sys.exit(1)


def validate_exists_or_exit(path):
  if not os.path.exists(path):
    print("ERROR: {0} does not exist".format(path), file=sys.stderr)
    sys.exit(1)


def is_int(i):
  try:
    int(i)
  except ValueError:
    return False
  else:
    return True


def mkdir_p(path):
  try:
    os.makedirs(path)
  except OSError as e:
    if e.errno == errno.EEXIST and os.path.isdir(path):
      pass
    else:
      raise
