from __future__ import absolute_import, print_function

import os.path
import tempfile
import shutil
import sys

from ... import utils, sftp

from .base_cmd import BaseExportCommand
from .exporter import associate_timesteps_with_filenames


class SFTPExportCommand(BaseExportCommand):
  description = "Export data with a templated CSE file from trn files on a SFTP server"

  def validate_args(self):
    BaseExportCommand.validate_args(self)

    if not self.args.result_file_only:
      if not self.args.trn_directory:
        print("ERROR: --trn-directory is required", file=sys.stderr)
        sys.exit(1)

      if not sftp.path_exists(self.args.trn_directory):
        print("ERROR: {0} does not exists on sftp server".format(self.args.trn_directory))
        sys.exit(1)

      self._remove_trn_tmp_directory = False
      if self.args.trn_tmp_directory:
        utils.validate_dir_or_exit(self.args.trn_tmp_directory)
      else:
        self.args.trn_tmp_directory = tempfile.mkdtemp()
        self._remove_trn_tmp_directory = True

      self.logger.info("using temporary directory {0} for downloading trn files".format(self.args.trn_tmp_directory))

  def transient_file_paths(self):
    with sftp.client() as c:
      fnames = associate_timesteps_with_filenames(
        c.listdir(self.args.trn_directory),
        filter_with_timesteps=self.args.timesteps,
        logger=self.logger
      )

      self.logger.info("found {0} matching timesteps to download".format(len(fnames)))

    # Long running connection will cause timeouts and hangs. Especially when it
    # takes 15 minutes for CFX to export something.
    # We should disconnect after download and reconnect.
    # TODO: in the future we can batch download a bunch, process them, and then whatever.

    for timestep, filename in fnames:
      source_path = os.path.join(self.args.trn_directory, filename)
      target_path = os.path.join(self.args.trn_tmp_directory, filename)
      with sftp.client() as c:
        self.logger.info("downloading timestep {0} from {1} to {2}...".format(timestep, source_path, target_path))
        c.get(source_path, target_path)

      yield timestep, target_path

  def export_complete(self, timestep, path):
    self.logger.info("done exporting {0}, removing {1}".format(timestep, path))
    os.remove(path)

  def after_run(self):
    BaseExportCommand.after_run(self)
    if self._remove_trn_tmp_directory:
      shutil.rmtree(self.args.trn_tmp_directory, ignore_errors=True)
