from __future__ import print_function, absolute_import

import logging
import json
import sys

from queue import Queue
from threading import Thread

from ... import notifiers
from ... import utils

from .exporter import Exporter


class BaseExportCommand(object):
  def __init__(self, parser):
    parser.add_argument("cse_template", help="the template cse file")
    parser.add_argument("-r", "--result-file", type=str, help="path to the res file or backup file where the mesh can be loaded")
    parser.add_argument("-t", "--timesteps", default="*", help="inclusive range of timesteps. format: a[-b][-every]. example: 2-200 (inclusive from 2 to 200), 150 (only 150), 2-200-50 (inclusive from 2-200, but only on 2, 52, 102, 152, or every 50). Using * exports for all it can find")
    parser.add_argument("-d", "--trn-directory", type=str, help="the directory of the transient files (could be local or remote)")
    parser.add_argument("--trn-tmp-directory", default=None, nargs="?", type=str, help="a directory to store the trn files temporarily (local, will create if not exists)")
    parser.add_argument("--result-file-only", action="store_true", help="if this is specified, export data only occurs on the result file and non of the transient file will be loaded.")
    parser.add_argument("-e", "--export-directory", type=str, help="the directory the output is supposed to go to")
    parser.add_argument("-p", "--parallel", type=int, default=1, help="the number of parallel workers to spawn. default: 1 (serial operations only)")
    parser.add_argument("-m", "--max-waiting", type=int, default=1, help="the (maximum number - 1) of steps that can be waiting to be processed at a time. default: 1. i.e. there is one step processing and TWO additional steps will be downloaded waiting to be processed by default. this means the maximum number of transient files downloaded at a time is parallel + max_waiting + 1 if using the sftp exporter. 0 here means infinity and is a bad idea")
    parser.add_argument("--extra-data-file", type=str, default=None, help="any extra data json file")
    parser.add_argument("--notify-when-done", action="store_true", help="if this is specified, it will attempt to use the notifier backend to notify when this is done")
    self.parser = parser

    self.logger = logging.getLogger("exporter")
    self.logger.setLevel(logging.DEBUG)

    self.extra_data = {}

  def validate_args(self):
    utils.validate_file_or_exit(self.args.cse_template)
    utils.validate_file_or_exit(self.args.result_file)
    utils.validate_dir_or_exit(self.args.export_directory)
    if self.args.trn_tmp_directory:
      utils.mkdir_p(self.args.trn_tmp_directory)

    if self.args.extra_data_file:
      utils.validate_file_or_exit(self.args.extra_data_file)

    timesteps = self.args.timesteps.split("-")
    timesteps = list(map(lambda s: s.strip(), timesteps))
    if len(timesteps) == 1:
      if timesteps[0] != "*" and not utils.is_int(timesteps[0]):
        print("ERROR: single timestep must be either * or a number.", file=sys.stderr)
        sys.exit(1)

      if timesteps[0] != "*":
        self.args.timesteps = set([int(timesteps[0])])

    elif len(timesteps) == 2:
      if not (utils.is_int(timesteps[0]) and utils.is_int(timesteps[1])) or int(timesteps[0]) < 0 or int(timesteps[1]) < 0:
        print("ERROR: timestep range must be numbers >= 0.", file=sys.stderr)
        sys.exit(1)

      self.args.timesteps = set(range(int(timesteps[0]), int(timesteps[1]) + 1))
    elif len(timesteps) == 3:
      if not (utils.is_int(timesteps[0]) and utils.is_int(timesteps[1])) or int(timesteps[0]) < 0 or int(timesteps[1]) < 0:
        print("ERROR: timestep range must be numbers >= 0.", file=sys.stderr)
        sys.exit(1)

      if not utils.is_int(timesteps[2]) or int(timesteps[2]) <= 0:
        print("ERROR: timestep every must be a number > 0", file=sys.stderr)
        sys.exit(1)

      self.args.timesteps = set(range(int(timesteps[0]), int(timesteps[1]) + 1, int(timesteps[2])))
    else:
      print("ERROR: timesteps must be either a number, *, or a range like 2-200", file=sys.stderr)
      sys.exit(1)

    if self.args.notify_when_done and not notifiers.default.available():
      print("WARNING: no notifications will be sent because {} support is not available".format(notifiers.default.name()))

  def run(self, args):
    self.args = args
    self.validate_args()

    if self.args.extra_data_file:
      with open(self.args.extra_data_file) as f:
        self.extra_data = json.load(f)

    self.exporter = Exporter(result_file=self.args.result_file,
                             export_directory=self.args.export_directory,
                             cse_template=self.args.cse_template)

    if not self.args.result_file_only:
      self.workers = []
      self.queue = Queue(maxsize=self.args.max_waiting)
      for i in range(self.args.parallel):
        self.logger.info("starting worker {0}".format(i))
        worker = Thread(target=self.export_worker, args=(i, ))
        worker.daemon = True
        worker.start()
        self.workers.append(worker)

      first = True
      for timestep, path in self.transient_file_paths():
        self.logger.info("queuing timestep {0}".format(timestep))
        self.queue.put((timestep, path, first))
        first = False

      for _ in self.workers:
        self.queue.put("done")

      self.logger.info("all jobs queued, waiting for queue to join")
      self.queue.join()

      self.logger.info("queue empty, joining workers")
      for worker in self.workers:
        worker.join()

    else:
      self.exporter.export_from_result_file(data=self.extra_data)

    self.after_run()

  def process_single_timestep(self, timestep, path, first):
    self.logger.info("processing timestep {0} at {1}...".format(timestep, path))
    self.exporter.export_from_transient_file(timestep, path, first=first, data=self.extra_data)
    self.export_complete(timestep, path)

  def export_worker(self, worker_num):
    while True:
      data = self.queue.get()
      if data == "done":
        self.queue.task_done()
        self.logger.info("worker {0} received done, exitting".format(worker_num))
        return

      timestep, path, first = data
      self.process_single_timestep(timestep, path, first)
      self.queue.task_done()

  def export_complete(self, timestep, path):
    # by default do nothing
    pass

  def transient_file_paths(self):
    raise NotImplementedError

  def notify_msg(self):
    return """
Exporting {0} completed for timesteps:

- {1}

    """.strip().format(self.args.result_file, "\n -".join(list(map(str, sorted(self.args.timesteps)))))

  def after_run(self):
    self.logger.info("run completed, executing after run hooks")
    if self.args.notify_when_done:
      notifiers.default.notify("Export job complete", self.notify_msg(), tags=["research", "done"])
