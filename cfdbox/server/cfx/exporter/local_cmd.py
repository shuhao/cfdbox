from __future__ import absolute_import

import os

from ... import utils

from .base_cmd import BaseExportCommand
from .exporter import associate_timesteps_with_filenames


class LocalExportCommand(BaseExportCommand):
  description = "Export data with a templated CSE file with locally available trn files."

  def validate_args(self):
    BaseExportCommand.validate_args(self)
    if not self.args.result_file_only:
      utils.validate_dir_or_exit(self.args.trn_directory)

  def transient_file_paths(self):
    fnames = associate_timesteps_with_filenames(
      os.listdir(self.args.trn_directory),
      filter_with_timesteps=self.args.timesteps,
      logger=self.logger
    )

    for timestep, filename in fnames:
      yield timestep, os.path.join(self.args.trn_directory, filename)
