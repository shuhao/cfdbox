
import os
import os.path
import subprocess

import jinja2

from ..utils import get_timestep_from_trn_filename


def associate_timesteps_with_filenames(filenames, filter_with_timesteps="*", logger=None):
  timesteps = {}
  for fname in filenames:
    try:
      timestep = get_timestep_from_trn_filename(fname)
    except ValueError:
      if logger:
        logger.warn("not sure what this file is: {0}".format(fname))

      continue
    else:
      timesteps[timestep] = fname

  if filter_with_timesteps and filter_with_timesteps != "*":
    for timestep in list(timesteps.keys()):
      if timestep not in filter_with_timesteps:
        del timesteps[timestep]

  timesteps = list(timesteps.items())
  timesteps.sort(key=lambda x: x[0])
  return timesteps


def filename_to_casename(path):
  basename = os.path.splitext(os.path.basename(path))[0]
  return basename.replace("-", " ")


class Exporter(object):
  def __init__(self, result_file, export_directory, cse_template, subprocess_call_func=subprocess.check_call):
    """Initializes an Exporter class.

    subprocess_call_func is merely for tests..
    """
    self.result_file = result_file
    self.export_directory = export_directory
    self.cse_template = cse_template
    self.subprocess_call_func = subprocess_call_func

  def export_from_transient_file(self, timestep, trnpath, first=True, data={}):
    context = {
      "export_directory": self.export_directory,
      "trn_filepath": trnpath,
      "timestep": timestep,
      "first": first
    }
    context.update(data)

    self._export_with_cse(context, timestep)

  def export_from_result_file(self, data={}):
    context = {
      "export_directory": self.export_directory,
    }

    context.update(data)

    self._export_with_cse(context, os.path.splitext(os.path.basename(self.result_file))[0])

  def _export_with_cse(self, context, cse_name_postfix):
    # Ansys likes to lock this file so we cannot export in parallel.
    # Linking should get around it?
    if "timestep" in context:
      result_filename, result_fileext = os.path.splitext(os.path.basename(self.result_file))
      result_filename = result_filename + str(context["timestep"]) + result_fileext
      result_file = os.path.join(os.path.dirname(self.result_file), result_filename)
      os.link(self.result_file, result_file)
      remove_result_file = True
    else:
      remove_result_file = False
      result_file = self.result_file

    context["result_file"] = result_file
    context["result_casename"] = filename_to_casename(result_file)

    rendered_cse_file = self._render_jinja2(self.cse_template, context)
    cse_filename = os.path.join(self.export_directory, "export_{0}.cse".format(cse_name_postfix))
    with open(cse_filename, "w") as f:
      f.write(rendered_cse_file)

    self.subprocess_call_func(["cfx5post", "-batch", cse_filename])

    os.remove(cse_filename)
    if remove_result_file:
      os.remove(result_file)

  def _render_jinja2(self, template_path, context):
    path, filename = os.path.split(template_path)
    return jinja2.Environment(loader=jinja2.FileSystemLoader(path)).get_template(filename).render(context)
