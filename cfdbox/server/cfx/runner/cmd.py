from __future__ import absolute_import, print_function

from contextlib import contextmanager
from datetime import datetime
import errno
import getpass
import os
import subprocess
import shutil
import sys
import tempfile
import time

from builtins import input

from ... import notifiers

TMP_BASE = "/tmp/{0}".format(getpass.getuser())
STARTING_MESSAGE_BODY = """
CFX job starting with the following command:

$ %s
""".strip()


def mkdir_p(p):
  try:
    os.makedirs(p)
  except OSError as exc:
    if exc.errno == errno.EEXIST and os.path.isdir(p):
      pass


def tail(f, n):
  stdout = os.popen("tail -n {0} {1}".format(n, f))
  lines = stdout.read()
  stdout.close()
  return lines


@contextmanager
def new_tmp_sshkey():
  mkdir_p(TMP_BASE)
  sshdir = tempfile.mkdtemp(dir=TMP_BASE)
  tmpid_path = os.path.join(sshdir, "tmpid")
  authorized_keys_path = os.path.join(os.environ["HOME"], ".ssh", "authorized_keys")

  p = subprocess.Popen(["ssh-keygen", "-t", "rsa", "-f", tmpid_path, "-N", ""], stdout=subprocess.PIPE)
  _, _ = p.communicate()
  retcode = p.poll()

  if retcode != 0:
    raise RuntimeError("cannot generate ssh key: {0}".format(retcode))

  with open(tmpid_path + ".pub") as f:
    pubkey = f.read()

  with open(authorized_keys_path) as f:
    old_authorized = f.read().strip()

  current_authorized = old_authorized + "\n" + pubkey

  with open(authorized_keys_path, "w") as f:
    f.write(current_authorized)

  yield tmpid_path

  with open(authorized_keys_path, "w") as f:
    f.write(old_authorized)

  shutil.rmtree(sshdir)


class BaseRunCommand(object):
  def __init__(self, parser):
    """This is shared between both the RunCommand and the ActualRunCommand"""
    parser.add_argument("-d", "--def-file", required=True, help="the definition file to run with")
    parser.add_argument("-k", "--keypath", nargs="?", default=None, help="DO NOT USE THIS OPTION. this option is used by the run command automatically.")
    parser.add_argument("-c", "--continue-file", nargs="?", default=None, help="a res file to continue with")
    parser.add_argument("-m", "--continue-method", nargs="?", default="ini-file", help="the continuation method. use the flag name as specified by cfx5solve")
    parser.add_argument("-q", "--quiet", action="store_true", help="do not ask for any prompt")
    self.parser = parser

  def validate_args(self, args):
    if not os.path.isfile(args.def_file):
      print("ERROR: {0} is not a valid file".format(args.def_file), file=sys.stderr)
      sys.exit(1)

    if args.continue_file is not None and not os.path.isfile(args.continue_file):
      print("ERROR: {0} is not a valid file".format(args.continue_file), file=sys.stderr)
      sys.exit(1)

    if not os.environ["CFX_LICENSE"]:
      print("ERROR: environmental variable $CFX_LICENSE must be defined.", file=sys.stderr)
      sys.exit(1)

    if not os.environ["CFX_NODES"]:
      print("ERROR: environmental variable $CFX_NODES must be defined.", file=sys.stderr)
      sys.exit(1)

  def run(self, args):
    self.validate_args(args)
    self.perform_action(args)


class RunCommand(BaseRunCommand):
  """This command is the wrapper to start the simulation.

  The reason it is separated is because CFX needs to use ssh-agent to identify
  which SSH key to use to connect to the other host.

  So this command does not actually do anything besides spawn another process
  that actually goes and starts CFX. However, we don't call actual-run directly
  because SSH keys must be setup.
  """
  description = "Start CFX simulations with this command."

  def confirm_settings(self, args):
    print("CFX will start in 5 seconds. You should ensure this is executing background")
    print("in the background.")
    print("")
    print("Things to do:")
    print("")
    print("1. Run the monitoring script in a separate location to monitor.")
    print("2. Run the copy script which will ocassionally copy transient files to the server.")
    print("")
    print("{0} notification support: {1}".format(notifiers.default.name(), "available" if notifiers.default.available() else "not avaliable"))
    print("")
    print("Run setting is the following:")
    print("")
    # Pretty print the options
    lines = []
    longestline = 0
    options = vars(args)
    options.update({"nodes": os.environ["CFX_NODES"], "license": os.environ["CFX_LICENSE"]})
    for k, v in options.items():
      if k in ("func", "which"):
        continue

      line = "{0}: {1}".format(k, v)
      if len(line) > longestline:
        longestline = len(line)

      lines.append(line)
    print("=" * longestline)
    for line in lines:
      print(line)
    print("=" * longestline)
    print("")
    print("Press CTRL+C to cancel")
    time.sleep(5)

  def perform_action(self, args):
    self.confirm_settings(args)
    with open("last_start_time", "w") as f:
      f.write(datetime.now().isoformat())

    actual_run_cmd = [
      "/usr/bin/ssh-agent",
      "cs-cfx",
      "actual-run",
      "-d", args.def_file
    ]

    if args.continue_file is not None:
      actual_run_cmd.append("--continue-file")
      actual_run_cmd.append(args.continue_file)
      actual_run_cmd.append("--continue-method")
      actual_run_cmd.append(args.continue_method)

    with new_tmp_sshkey() as keypath:
      actual_run_cmd.append("-k")
      actual_run_cmd.append(keypath)
      retcode = subprocess.check_call(actual_run_cmd)
      sys.exit(retcode)


class ActualRunCommand(BaseRunCommand):
  """This command actually starts cfx5solve.

  It also checks the simulation a little bit, ensuring everything is correct.
  """
  description = "DO NOT CALL THIS MANUALLY. Use the run command instead"

  def ending_message_body(self, def_file, cmd, retcode):
    basedir = os.path.dirname(os.path.abspath(def_file))

    outfiles = []
    for p in os.listdir(basedir):
      if p.endswith(".out") and os.path.isfile(os.path.join(basedir, p)):
        outfiles.append(os.path.join(basedir, p))

    outfiles.sort()

    return """
  CFX job started with %s has completed with exit code %d.

  The last 200 lines of out file is:

  %s
    """.strip() % (cmd, retcode, tail(outfiles[-1], 200))

  def check_definition(self):
    if self.args.quiet:
      print("WARNING: definition file will not be checked for paths")
      return

    def_dir = os.path.dirname(self.args.def_file)
    ccl_file = os.path.join(def_dir, self.args.def_file + ".ccl")
    if os.path.exists(ccl_file):
      print("Found a file at {0}. This file must be overwritten from a copy generated from the current definition file".format(ccl_file))
      print("If you have unsaved changes, select N for the following option.")
      confirm = input("overwrite {0}? (must select yes to run simulation) [y/N] ".format(ccl_file))
      if confirm.strip().lower() == "y":
        os.remove(ccl_file)
      else:
        print("ERROR: You must select yes to continue.")
        print("")
        sys.exit(0)

    subprocess.check_call(["cfx5cmds", "-read", "-def", self.args.def_file, "-text", ccl_file])

    filenames = []
    with open(ccl_file) as f:
      current_filename = None
      for line in f:
        line = line.strip()

        if line.startswith("File Name"):
          potential = line.split("=", 1)[1].strip()
          if potential.endswith("\\"):
            current_filename = potential.strip("\\")
          else:
            filenames.append(potential)
            current_filename = None
        elif current_filename is not None:
          current_filename += line.strip("\\")
          if not line.endswith("\\"):
            filenames.append(current_filename)
            current_filename = None

    if len(filenames) > 0:
      print("Found some file names. Are these paths correct (if they match they should be correct)?")

      for fn in filenames:
        splitted = fn.replace("\\", "/").split(":", 1)
        suspected = os.path.join(os.path.dirname(os.path.abspath(self.args.def_file)), os.path.basename(splitted[-1]))

        if not os.path.exists(suspected):
          found_msg = "(not found here)"
        else:
          found_msg = "(file exists here)"

        print("  - Actual:    {0}".format(fn))
        print("  - Suspected: {0} {1}".format(suspected, found_msg))
        print("")

      confirm = input("if so, type y here, otherwise please go adjust it and run this script again [y/N]: ")
      if confirm.strip().lower() != "y":
        print("After adjustment, run `cfx5cmds -write -def {0} -text {0}.ccl`".format(os.path.basename(self.args.def_file)))
        print("Re-run this script after and hit y.")
        sys.exit(0)

  def perform_action(self, args):
    self.args = args

    subprocess.check_call(["/usr/bin/ssh-add", args.keypath])

    self.check_definition()

    cfx_cmd = [
      "cfx5solve",
      "-v",
      "-def", args.def_file,
      "-start-method", "Platform MPI Distributed Parallel",
      "-par",
      "-par-dist", os.environ["CFX_NODES"]
    ]

    if args.continue_file is not None:
      cfx_cmd.append("-{0}".format(args.continue_method))
      cfx_cmd.append(args.continue_file)

    display_cmd = " ".join(cfx_cmd)

    notifiers.default.notify("CFX Job starting", STARTING_MESSAGE_BODY % display_cmd, tags=["restart", "start"])
    print("running cfx: {0}".format(display_cmd))

    os.chdir(os.path.dirname(os.path.abspath(args.def_file)))

    proc = subprocess.Popen(cfx_cmd)
    with open("cfx.pid", "w") as f:
      print("{0}:cfx".format(proc.pid), file=f)

    retcode = proc.wait()

    notifiers.default.notify("CFX Job complete", self.ending_message_body(args.def_file, display_cmd, retcode), tags=["research", "done"])
    os.remove("cfx.pid")
