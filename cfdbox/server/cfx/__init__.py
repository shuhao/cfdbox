from __future__ import absolute_import

from .runner import RunCommand, ActualRunCommand
from .trnmover import TrnMoverCommand
from .monitor import MonitorCommand
from .exporter import LocalExportCommand, SFTPExportCommand
from .sftphelpers import SFTPGetCommand


commands = {
  "run":          RunCommand,
  "actual-run":   ActualRunCommand,
  "trn-mover":    TrnMoverCommand,
  "monitor":      MonitorCommand,
  "local-export": LocalExportCommand,
  "sftp-export":  SFTPExportCommand,
  "sftp-get":     SFTPGetCommand
}
