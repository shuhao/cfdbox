from __future__ import absolute_import, print_function

import time
from io import StringIO

from datetime import datetime

from ... import notifiers

from .checks import transient_checks


class Monitor(object):
  def __init__(self, args, logger, checks):
    self.args = args
    self.logger = logger
    self.enabled_checks = [c(vars(args), logger) for c in checks]
    self.disabled_checks = set()  # set of class names
    self._first = True  # so we don't send out a daily report right away

  def disable_checks_if_neccesary(self):
    self.enabled_checks = [c for c in self.enabled_checks if c.__class__.__name__ not in self.disabled_checks]

  def run(self):
    minutes = 1
    self.check("@minutely")
    self.check("@hourly")
    self.check("@daily")
    self.daily_report()

    while True:
      self.check("@minutely")
      if minutes % 60 == 0:
        self.check("@hourly")

      if minutes % 1440 == 0:
        self.check("@daily")
        self.daily_report()

      time.sleep(60)
      minutes += 1

  def check(self, activates_every):
    for check in self.enabled_checks:
      if check.activates_every != activates_every:
        continue

      check_name = check.__class__.__name__

      # Disable checks if they don't pass or errors
      try:
        passed, message = check.check()
      except Exception as e:
        notifiers.default.notify_exception("{0} raised an exception".format(check_name), e)
        self.logger.error("{0} raised an exception: {1}".format(check_name, e))
        self.disabled_checks.add(check_name)
        continue

      if passed:
        if message:
          self.logger.info("{0}: {1}".format(check_name, message))
      else:
        self.disabled_checks.add(check_name)
        self.logger.error("failed check {0}: {1}".format(check_name, message))
        notifiers.default.notify("[CFX Monitor] Failed check {0}".format(check_name), message, tags=["cfx", "error"])

      if check.disabled:
        self.disabled_checks.add(check_name)

    self.disable_checks_if_neccesary()

  def daily_report(self):
    messages = {}
    for check in self.enabled_checks:
      check_name = check.__class__.__name__

      try:
        message = check.daily_report()
      except Exception as e:
        notifiers.default.notify_exception("{0} raised an exception".format(check_name), e)
        self.logger.error("{0} raised an exception: {1}".format(check_name, e))
        self.disabled_checks.add(check_name)
        continue

      if message:
        messages[check_name] = message

      if check.disabled:
        self.disabled_checks.add(check_name)

    if len(messages) > 0 and not self._first:
      report = StringIO()

      title = "Run report for {0}".format(self.args.outfile)
      print(title,            file=report)
      print(len(title) * "=", file=report)
      print("",               file=report)

      for check_name, message in messages.items():
        message = message.strip()
        print(check_name,            file=report)
        print(len(check_name) * "-", file=report)
        print("",                    file=report)
        print(message,               file=report)
        print("",                    file=report)
        print("",                    file=report)

      print("Report generated on: {0}".format(datetime.utcnow().isoformat()), file=report)

      emailbody = report.getvalue()
      report.close()

      notifiers.default.notify("Daily CFX run report for {0}".format(self.args.outfile), emailbody, tags=["cfx", "daily-report"], channel="daily")

    self._first = False

    self.disable_checks_if_neccesary()


class TransientMonitor(Monitor):
  def __init__(self, args, logger):
    Monitor.__init__(self, args, logger, transient_checks)
