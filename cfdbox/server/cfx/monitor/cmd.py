from __future__ import absolute_import, print_function

import logging
import sys

from ... import utils

from .monitor import TransientMonitor


# TODO: this needs to be made way more modular
class MonitorCommand(object):
  description = "Monitor CFX run."

  def __init__(self, parser):
    self.logger = logging.getLogger("monitor")
    self.logger.setLevel(logging.DEBUG)

    parser.add_argument("type", choices=["transient"], help="the type of runs to monitor")
    parser.add_argument("outfile", help="the out number to monitor")
    parser.add_argument("outdir", help="the out directory where transient files are being written to")
    parser.add_argument("--timestep-milestones", nargs="*", default=[], help="milestones in number of timesteps. this script can then email about estimated time to finish")
    parser.add_argument("--low-space-threshold-in-gb", nargs="?", type=int, default=20, help="GB of space left before alert kicks in")
    parser.add_argument("--pid-file", nargs="?", default="cfx.pid", help="cfx pid file path")
    parser.add_argument("--daily-export-cse-template", nargs="?", default=None, help="export cse template for exporting images/data daily for report emails. this cse must export to the export directory specific a picture named dailyreport.png and/or a report text file of report.txt")
    parser.add_argument("--daily-export-upload-directory", nargs="?", default="", help="the directory to upload to. example: simulation1, simulations/sim1-3d")

    self.parser = parser

  def validate_args(self):
    utils.validate_file_or_exit(self.args.outfile)
    utils.validate_dir_or_exit(self.args.outdir)
    utils.validate_file_or_exit(self.args.pid_file)
    if self.args.daily_export_cse_template:
      utils.validate_file_or_exit(self.args.daily_export_cse_template)

    # need a post fix of /
    if not self.args.daily_export_upload_directory.endswith("/"):
      self.args.daily_export_upload_directory += "/"

    # but cannot have a prefix of /
    if self.args.daily_export_upload_directory.startswith("/"):
      self.args.daily_export_upload_directory = self.args.daily_export_upload_directory[1:]

    for i, t in enumerate(self.args.timestep_milestones):
      try:
        t = int(t)
        if t < 0:
          raise ValueError
      except ValueError:
        print("ERROR: {0} is not a valid timestep".format(t), file=sys.stderr)
        sys.exit(1)
      else:
        self.args.timestep_milestones[i] = t

    self.args.timestep_milestones.sort()

  def run(self, args):
    self.args = args
    self.validate_args()

    monitor = TransientMonitor(self.args, self.logger)
    monitor.run()
