from __future__ import absolute_import

import os

from .base_check import BaseCheck


class ProcessCheck(BaseCheck):
  """Checks if a process is alive"""

  @staticmethod
  def check_if_pid_exists(pid):
    try:
      os.kill(pid, 0)
    except OSError:
      return False
    else:
      return True

  def __init__(self, *args):
    BaseCheck.__init__(self, *args)
    self.activates_every = "@minutely"
    self.logger.info("will be checking the following pids:")
    for pid, name in self.get_pids():
      self.logger.info("{0}: {1}".format(name, pid))
    self.logger.info("this list may change if the pid file gets changed")

  def get_pids(self):
    pids = []
    with open(self.config["pid_file"]) as f:
      for line in f:
        pid, name = tuple(line.strip().split(":"))
        pid = int(pid)
        pids.append((pid, name))

    return pids

  def check(self):
    # It is possible the runner deletes this file...
    # in which case we stop monitoring
    if os.path.isfile(self.config["pid_file"]):
      pids = self.get_pids()
      for pid, name in pids:
        if not self.check_if_pid_exists(pid):
          return False, "{0} stopped? PID {1} does not exist anymore".format(name, pid)

      return True, ""
    else:
      return True, "no longer watching any PIDs as the pid file got deleted"
