from __future__ import absolute_import, print_function

from io import StringIO
import os.path
import shutil
import tempfile

from .... import minb2

from .base_check import BaseCheck

from ...utils import get_last_backup_file, get_timestep_from_trn_filename
from ...exporter import Exporter


class ScreenshotReporter(BaseCheck):
  def __init__(self, *args, **kwargs):
    BaseCheck.__init__(self, *args, **kwargs)

    if not self.config["daily_export_cse_template"]:
      self.logger.warn("screenshot reporter disabled due to a lack of cse template")
      self.disabled = True
      return

    self.b2client = minb2.B2()

  def upload_image(self, png_path, filename):
    return self.b2client.upload_file(png_path, filename=filename)

  def daily_report(self):
    filenames = os.listdir(self.config["outdir"])
    backup_file = get_last_backup_file(filenames)
    if not backup_file:
      msg = "no report was generated as backup file was not found in: {0}".format(filenames)
      self.logger.warn(msg)
      return msg

    backup_file = os.path.join(self.config["outdir"], backup_file)

    # We want to hardlink a copy so CFX doesn't delete it while we are processing
    #
    # Technically, the Linux kernel should still keep the file we have opened
    # around. However, let's just do it for extra caution.
    tmpbasedir = os.path.join(os.environ["CFDBOX_ROOT"], "tmp")
    tmpdir = tempfile.mkdtemp(dir=tmpbasedir)

    tmp_backup_filepath = os.path.join(tmpdir, os.path.basename(backup_file))
    os.link(backup_file, tmp_backup_filepath)

    self.logger.info("exporting report from cfx5post")
    exporter = Exporter(tmp_backup_filepath, tmpdir, self.config["daily_export_cse_template"])
    exporter.export_from_result_file()

    report_text = None
    image_link = None

    report_png_path = os.path.join(tmpdir, "dailyreport.png")
    if os.path.exists(report_png_path):
      self.logger.info("uploading report image")
      timestep = get_timestep_from_trn_filename(backup_file, ext=".bak")
      filename = "{0}{1}.png".format(self.config["daily_export_upload_directory"], timestep)
      image_link = self.upload_image(report_png_path, filename)
      self.logger.info("done uploading images, available at: {0}".format(image_link))
    else:
      self.logger.warn("did not find image at {0}".format(report_png_path))
      self.logger.warn("only see: {0}".format(os.listdir(tmpdir)))

    report_file_path = os.path.join(tmpdir, "report.txt")
    if os.path.exists(report_file_path):
      with open(report_file_path) as f:
        report_text = f.read()

    report = StringIO()
    if image_link:
      print("An image has been generated and it is available at:", file=report)
      print("Image link: {0}".format(image_link),                  file=report)
      print("The file from which it is generated: {0}".format(os.path.basename(backup_file)), file=report)
      print("",                                                    file=report)

    if report_text:
      print(report_text, file=report)

    report_full_txt = report.getvalue()
    report.close()

    shutil.rmtree(tmpdir, ignore_errors=True)
    self.logger.info("done doing report screenshot")

    return report_full_txt
