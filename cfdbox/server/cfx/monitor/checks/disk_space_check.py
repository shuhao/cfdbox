from __future__ import absolute_import

import subprocess

from .base_check import BaseCheck


class InvalidDfOutput(Exception):
  pass


class DiskSpaceCheck(BaseCheck):
  """Checks if the CFX process is alive"""

  @staticmethod
  def check_diskspace_remaining(location):
    df = subprocess.Popen(["df", location], stdout=subprocess.PIPE)
    output = df.communicate()[0]
    output = output.decode("utf-8")
    device, size, used, available, percent, mountpoint = output.split("\n")[1].split()
    # we'll let it crash and be absorbed by top level.

    try:
      return int(available)
    except ValueError:
      raise InvalidDfOutput("invalid output from df: {0}".format(output))

  def __init__(self, *args):
    BaseCheck.__init__(self, *args)
    self.activates_every = "@hourly"

  def check(self):
    space_remaining = self.check_diskspace_remaining(self.config["outdir"])

    # We want this in gigabytes
    space_remaining = round(space_remaining / 1024.0 / 1024.0, 2)

    if space_remaining < self.config["low_space_threshold_in_gb"]:
      return False, "not enough space on disk. Only {0}GB left.".format(space_remaining)

    return True, "disk space remaining: {0}GB".format(space_remaining)
