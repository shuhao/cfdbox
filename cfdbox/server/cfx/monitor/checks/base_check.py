class BaseCheck(object):
  """This is the base check class.

  The checks will be instantized once and kept until shutdown. You can
  store state in the object without worrying that it will go away in
  the next check.

  If a check raises, it will be disabled as well.
  """

  def __init__(self, config, logger):
    self.config = config
    self.logger = logger

    # @minutely
    # @hourly
    # @daily
    # None => no activation, for daily_report type checks
    self.activates_every = None

    # If you want to be disabled but don't want to notify via the notifier
    self.disabled = False

  def check(self):
    """Checks whatever criteria it sees fit.

    When a check fails, it will be disabled.

    Returns:
      passed: bool for passed the check or failed the check
      message: a string to be passed to the notifier when the check fails, or logged when passed
    """
    return True, ""

  def daily_report(self):
    """Returns a string that will be sent out for daily reporting email.

    Can perform calculation here.
    """
    return None
