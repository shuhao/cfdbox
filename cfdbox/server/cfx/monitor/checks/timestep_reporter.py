from __future__ import absolute_import

import os

from .base_check import BaseCheck

from ...utils import get_current_timestep_from_filenames, OutdirNotFound


def humanize_time(timedelta_in_hour):
  if timedelta_in_hour < 1:
    t = "about {0} minutes".format(round(timedelta_in_hour * 60))
  elif timedelta_in_hour < 50:
    t = "about {0} hours".format(round(timedelta_in_hour))
  elif timedelta_in_hour < 170:
    t = "about {0} days".format(round(timedelta_in_hour / 24))
  else:
    t = "about {0} weeks".format(round(timedelta_in_hour / 168))

  return "in {0}".format(t)


class TimestepReporter(BaseCheck):
  def __init__(self, *args):
    BaseCheck.__init__(self, *args)

    self._yesterday_last_timestep = self.get_current_timestep()

  def get_current_timestep(self):
    if not os.path.isdir(self.config["outdir"]):
      raise OutdirNotFound("{0} cannot be found".format(OutdirNotFound))

    return get_current_timestep_from_filenames(os.listdir(self.config["outdir"]))

  def daily_report(self):
    info = {
      "current_timestep": None,
      "timestep_completed_since_previous": None,
      "timestep_speed_per_hour": None,
      "eta_til_next_milestone": None,
      "next_milestone": None
    }

    info["current_timestep"] = self.get_current_timestep()
    info["timestep_completed_since_previous"] = info["current_timestep"] - self._yesterday_last_timestep
    info["timestep_speed_per_hour"] = round(info["timestep_completed_since_previous"] / 24.0, 2) + 1

    for t in self.config["timestep_milestones"]:
      if t > info["current_timestep"]:
        info["next_milestone"] = t
        break

    if info["next_milestone"] is None:
      info["next_milestone"] = "no milestones remaining"
      info["eta_til_next_milestone"] = "no milestones remaining"
    else:
      info["eta_til_next_milestone"] = humanize_time(float(info["next_milestone"] - info["current_timestep"]) / info["timestep_speed_per_hour"])

    self._yesterday_last_timestep = info["current_timestep"]

    report = """
Current timestep                   : {current_timestep}
Timesteps completed since yesterday: {timestep_completed_since_previous}
Timesteps per hour                 : {timestep_speed_per_hour}
Next milestone                     : {next_milestone}
ETA until the next milestone       : {eta_til_next_milestone}
    """.strip().format(**info)

    return report
