from __future__ import absolute_import

from .disk_space_check import DiskSpaceCheck
from .process_check import ProcessCheck
from .timestep_reporter import TimestepReporter
from .screenshot_reporter import ScreenshotReporter

transient_checks = [
  DiskSpaceCheck,
  ProcessCheck,
  TimestepReporter,
  ScreenshotReporter
]
