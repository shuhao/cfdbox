from __future__ import absolute_import, print_function

import logging
import sys

from ... import sftp


class SFTPGetCommand(object):
  description = "get a file from sftp server with settings defined in the sftprc"

  def __init__(self, parser):
    self.logger = logging.getLogger("trn-mover")
    self.logger.setLevel(logging.DEBUG)

    parser.add_argument("remote_path", help="the remote path where the file is present")
    parser.add_argument("local_path", help="the local path where the file should go to")

  def validate_args(self, args):
    if not sftp.path_exists(args.remote_path):
      print("ERROR: {0} does not exists on sftp server".format(args.remote_path))
      sys.exit(1)

  def run(self, args):
    self.validate_args(args)
    with sftp.client() as c:
      self.logger.info("downloading from {0} to {1}...".format(args.remote_path, args.local_path))
      c.get(args.remote_path, args.local_path)
      self.logger.info("download complete.")
