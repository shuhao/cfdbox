import os


def get_timestep_from_trn_filename(path, ext=".trn"):
  fname = os.path.basename(path)
  if fname.endswith("_full{0}".format(ext)):
    return int(fname[:-9])
  elif fname.endswith(ext):
    return int(fname[:-4])
  else:
    raise ValueError("filename is not a transient file")


def get_current_timestep_from_filenames(filenames):
  max_timestep = 0
  for fn in filenames:
    try:
      current_timestep = get_timestep_from_trn_filename(fn)
    except ValueError:
      continue

    if current_timestep > max_timestep:
      max_timestep = current_timestep-1

  return max_timestep


def get_last_backup_file(filenames):
  max_timestep = 0
  filenames = set(filenames)

  for fn in filenames:
    try:
      current_timestep = get_timestep_from_trn_filename(fn, ext=".bak")
    except ValueError:
      continue

    if current_timestep > max_timestep:
      max_timestep = current_timestep

  full_filename = "{0}_full.bak".format(max_timestep)
  short_filename = "{0}.bak".format(max_timestep)
  if full_filename in filenames:
    return full_filename
  elif short_filename in filenames:
    return short_filename
  else:
    return None


class OutdirNotFound(Exception):
  pass
