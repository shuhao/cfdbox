from __future__ import absolute_import, print_function

import logging
import os
import os.path
import sys
import time

from ... import sftp


class TrnMoverCommand(object):
  description = "moves transients files periodically to a SFTP server"

  def __init__(self, parser):
    self.logger = logging.getLogger("trn-mover")
    self.logger.setLevel(logging.DEBUG)

    parser.add_argument("directory", help="directory where the transient files live")
    parser.add_argument("target_directory", help="the target directory on the sftp server. this must already exist on the server.")
    parser.add_argument("-i", "--interval", type=int, nargs="?", default=600, help="number of seconds between checks (default: 600s)")
    parser.add_argument("-n", "--number-of-files-to-keep", type=int, nargs="?", default=10, help="the number of latest transient files to keep on this machine (default: 10)")
    parser.add_argument("-d", "--delete", action="store_true", help="this option will enable deleting of trn files")
    self.parser = parser

  def validate_args(self):
    if not os.path.isdir(self.args.directory):
      print("ERROR: {0} is not a directory".format(self.args.directory), file=sys.stderr)
      sys.exit(1)

    if not sftp.available():
      print("ERROR: environmental variables SFTP_SERVER, SFTP_USER, and SFTP_PASSWORD are required", file=sys.stderr)
      sys.exit(1)

    if not sftp.path_exists(self.args.target_directory):
      print("ERROR: {0} does not exists on sftp server".format(self.args.target_directory))
      sys.exit(1)

  def scan_directory_for_files_to_move(self):
    trn_files = []
    bak_files = []
    for fn in os.listdir(self.args.directory):
      abs_fn_path = os.path.join(os.path.abspath(self.args.directory), fn)

      if fn.endswith(".trn"):
        trn_files.append((abs_fn_path, int(fn.split(".")[0])))  # the integer is for sorting...
      elif fn.endswith(".bak"):
        tmp_path = os.path.join(os.path.expanduser("~"), fn)
        if os.path.exists(tmp_path):
          self.logger.warn("{0} already exists, assume move was complete? skipping...".format(tmp_path))
          continue
        os.link(abs_fn_path, tmp_path)
        bak_files.append(tmp_path)

    trn_files.sort(key=lambda x: x[1])
    trn_files = trn_files[:-self.args.number_of_files_to_keep]

    files = []
    self.logger.info("files detected as below")
    for fn in trn_files:
      files.append(fn[0])
      self.logger.info(os.path.basename(fn[0]))

    for fn in bak_files:
      files.append(fn)
      self.logger.info(os.path.basename(fn))

    return files

  def move_files_to_sftp(self, files):
    with sftp.client() as client:
      client.chdir(self.args.target_directory)

      files_uploaded = set()
      for f in files:
        basefn = os.path.basename(f)
        self.logger.info("Uploading {0}".format(basefn))
        client.put(f, basefn)

        remote_stat = client.stat(basefn)
        local_stat = os.stat(f)

        if remote_stat.st_size != local_stat.st_size:
          raise RuntimeError("uploaded size does not match for {0} (expected: {1}, actual: {2})".format(basefn, local_stat.st_size, remote_stat.st_size))

        files_uploaded.add(basefn)

      entries = client.listdir(".")
      for entry in entries:
        files_uploaded.discard(entry)

      if len(files_uploaded) > 0:
        raise RuntimeError("some files were not uploaded: {0}".format(", ".join(files_uploaded)))

      self.logger.info("uploads completed and verified")

  def single_run(self):
    files_to_move = self.scan_directory_for_files_to_move()
    if len(files_to_move) == 0:
      self.logger.info("no files needs to be moved")
      return

    self.move_files_to_sftp(files_to_move)

    if self.args.delete:
      self.logger.warning("deleting the following files: {0}".format(files_to_move))
      for fn in files_to_move:
        os.remove(fn)
    else:
      self.logger.info("not deleting any files")

  def run(self, args):
    self.args = args
    self.validate_args()

    if os.path.isfile("cfx.pid"):
      self.logger.info("detected cfx.pid file, writing pid into it for monitoring")
      with open("cfx.pid", "r+") as f:
        content = []
        for line in f:
          line = line.strip()
          if "trn-mover" not in line:
            content.append(line)

        content.append("{0}:trn-mover".format(os.getpid()))

        f.seek(0)
        f.write("\n".join(content))
        f.truncate()

    while True:
      self.single_run()
      self.logger.info("done moving, going to sleep for {0}s".format(self.args.interval))
      time.sleep(self.args.interval)
