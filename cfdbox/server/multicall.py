from __future__ import absolute_import, print_function

import argparse
import os
import sys
import logging

from . import notifiers


# To define additional commands, implement a Commandlet-like class.
# It has an __init__ function that takes an argument subparser.
# It has a run function that takes in the argument passed through the subparser.
# This function will be bound by set_default() from the argparse module.


def main(commands):
  logging.basicConfig(format="[%(asctime)s][%(levelname)s] %(message)s", datefmt="%Y-%m-%d %H:%M:%S")
  if not os.environ.get("VIRTUAL_ENV"):
    print("ERROR: not running inside a virtualenv. Ensure you ran bootstrap and ensure you activated the setup (try logging out and logging back in).", file=sys.stderr)
    sys.exit(1)

  parser = argparse.ArgumentParser(description="{0} multicall executable".format(os.path.basename(sys.argv[0])))
  subparsers = parser.add_subparsers()
  for name, command_cls in commands.items():
    subparser = subparsers.add_parser(name, help=command_cls.description)
    command = command_cls(subparser)
    subparser.set_defaults(func=command.run, which=name)

  args = parser.parse_args()

  if len(vars(args)) == 0:
    # If you call this with no arguments, it will break like this:
    # https://bugs.python.org/issue16308
    parser.print_usage()
    print("{}: error: too few arguments".format(parser.prog), file=sys.stderr)
    sys.exit(1)

  try:
    args.func(args)
  except Exception as e:
    notifiers.default.notify_exception("cfdbox-server-cfx {0} crashed".format(args.which), e)
    raise e
