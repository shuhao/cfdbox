from __future__ import absolute_import, division

import csv
import os.path

from ..core.workunit import WorkUnit


class CsvAverager(WorkUnit):
  description = "averages a bunch of CSVs row by row"
  DEFAULT_LINES_TO_SKIP = 0

  @classmethod
  def initialize(cls, *args, **kwargs):
    super(CsvAverager, cls).initialize(*args, **kwargs)
    cls.parser.add_argument("--lines-to-skip", type=int, default=cls.DEFAULT_LINES_TO_SKIP, help="number of lines at the top of the files to skip (for headers and such). default: {}".format(cls.DEFAULT_LINES_TO_SKIP))
    cls.parser.add_argument("--has-headers", action="store_true", default=False, help="has headers")
    cls.parser.add_argument("--csv-files", nargs="+", required=True, type=str, help="path to the csv file")
    cls.parser.add_argument("--columns-to-ignore", nargs="*", default=[], type=int, help="the columns to not average")
    cls.parser.add_argument("--output-path", required=True, help="output file path")

  @classmethod
  def split_input_from_args(cls, args):
    global_data = vars(args)
    global_data["columns_to_ignore"] = set(global_data["columns_to_ignore"])

    return global_data, [True]

  @classmethod
  def validate_inputs(cls, global_data, local_datas):
    for csv_file in global_data["csv_files"]:
      if not os.path.isfile(csv_file):
        return False, "{} is not a valid file".format(csv_file)

    if global_data["lines_to_skip"] < 0:
      return False, "lines-to-skip must be a positive number"

    # TODO: more sanity checks.

    return True, ""

  def id(self):
    return "csv-averager"

  def work(self):
    all_filenames = map(lambda f: os.path.splitext(os.path.basename(f))[0], self._global_data["csv_files"])
    all_filenames = ", ".join(all_filenames)
    self.logger.info("averaging {}".format(all_filenames))

    data = []
    headers = None

    for i, fn in enumerate(self._global_data["csv_files"]):
      self.logger.info("processing file {}".format(fn))
      with open(fn) as f:
        reader = csv.reader(f)
        for line_num, line in self.tqdm(enumerate(reader), desc="{} lines".format(os.path.basename(fn))):
          if line_num < self._global_data["lines_to_skip"]:
            continue

          if line_num == self._global_data["lines_to_skip"] and self._global_data["has_headers"]:
            headers = line
            continue

          if i == 0:
            data.append(list(map(float, line)))
            continue

          data_line_num = line_num - self._global_data["lines_to_skip"]
          if self._global_data["has_headers"]:
            data_line_num -= 1

          for j, v in enumerate(map(float, line)):
            if j in self._global_data["columns_to_ignore"]:
              continue

            data[data_line_num][j] += v

    self.logger.info("averaging data")
    n = len(self._global_data["csv_files"])
    for line in data:
      for j, v in enumerate(line):
        if j not in self._global_data["columns_to_ignore"]:
          line[j] = v / n

        line[j] = "{:.8E}".format(line[j])

    self.logger.info("writing out to CSV")
    with open(self._global_data["output_path"], "w") as f:
      writer = csv.writer(f)
      if headers:
        writer.writerow(headers)

      writer.writerows(data)
