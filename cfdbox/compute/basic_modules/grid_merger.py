from __future__ import absolute_import, division

import csv
import os.path

from .csv_workunit import CsvWorkUnit
from ..utilities.structured_grid import StructuredGrid


class GridMerger(CsvWorkUnit):
  description = "merges the grid from many csv files and reorders them into structured"

  @classmethod
  def initialize(cls, *args, **kwargs):
    super(GridMerger, cls).initialize(*args, **kwargs)
    cls.parser.add_argument(
      "--has-headers", action="store_true", default=False,
      help="has headers"
    )

    cls.parser.add_argument(
      "--x-filter", nargs="*", default=[], type=float,
      help="the low x and high x to be filtered"
    )

    cls.parser.add_argument(
      "--y-filter", nargs="*", default=[], type=float,
      help="the low y and high y to be filtered"
    )

    cls.parser.add_argument(
      "--z-filter", nargs="*", default=[], type=float,
      help="the low z and high z to be filtered"
    )

    cls.parser.add_argument(
      "--minimum-grid-spacing", type=float, default=1e-6,
      help="the minimum grid spacing in any direction. default: 1e-6"
    )

    cls.parser.add_argument(
      "--output-path", required=True,
      help="the output from this"
    )

  @classmethod
  def validate_filter(cls, filter):
    if len(filter) == 0:
      return True, ""

    if len(filter) == 2:
      return True, ""

    return False, "filters must consists of 2 values in the format of: low high"

  @classmethod
  def validate_inputs(cls, global_data, local_datas):
    valid, message = cls.validate_filter(global_data["x_filter"])
    if not valid:
      return valid, message

    valid, message = cls.validate_filter(global_data["y_filter"])
    if not valid:
      return valid, message

    valid, message = cls.validate_filter(global_data["z_filter"])
    if not valid:
      return valid, message

    return True, ""

  @classmethod
  def all_work_done(cls, data):
    structured_grid = StructuredGrid(data[0][1]["global"]["minimum_grid_spacing"])
    headers = None
    for work_id, input_data, output_data in data:
      cls.logger.info("merging grid from {}".format(os.path.basename(work_id)))

      headers = output_data["headers"]
      for coord, line in output_data["rows"]:
        x, y, z = coord
        structured_grid.append(x, y, z, line)

    structured_grid.finalize()
    cls.logger.info("finalized structured grid with dimensions: {}".format(structured_grid.dimensions()))

    output_path = data[0][1]["global"]["output_path"]

    cls.logger.info("writting to {}".format(output_path))
    with open(output_path, "w") as f:
      writer = csv.writer(f)
      if headers:
        writer.writerow(headers)
      writer.writerows(structured_grid.as_rows())

  def preprocess(self):
    self._output["rows"] = []

  def process_line(self, linenum, line):
    if linenum == 0 and self._global_data["has_headers"]:
      self._output["headers"] = line
      return

    if len(line) < 3:
      raise ValueError("line {} has less than 3 values".format(linenum + self._global_data["lines_to_skip"]))

    x, y, z = float(line[0]), float(line[1]), float(line[2])

    if self._within_x_filter(x) and self._within_y_filter(y) and self._within_z_filter(z):
      self._output["rows"].append(([x, y, z], line[3:]))

  def _within_x_filter(self, x):
    return self._within_filter(x, self._global_data["x_filter"])

  def _within_y_filter(self, y):
    return self._within_filter(y, self._global_data["y_filter"])

  def _within_z_filter(self, z):
    return self._within_filter(z, self._global_data["z_filter"])

  def _within_filter(self, v, fi):
    if len(fi) == 0:
      return True

    return fi[0] <= v <= fi[1]
