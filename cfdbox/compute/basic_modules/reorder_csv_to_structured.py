from __future__ import absolute_import, division

import csv
import os.path

from .csv_workunit import CsvWorkUnit
from ..utilities.structured_grid import StructuredGrid


class ReorderCsvToStructured(CsvWorkUnit):
  description = "reorder a csv with x, y, z as its first 3 column such that it forms a structured grid taken by paraview/tecplot"

  @classmethod
  def initialize(cls, *args, **kwargs):
    super(cls, cls).initialize(*args, **kwargs)
    cls.parser.add_argument("--has-headers", action="store_true", default=False, help="if there is a header row after lines to ignore to keep writing")
    cls.parser.add_argument("--ignore-cfx-domain-lines", action="store_true", default=False, help="if lines with cfx domains")
    cls.parser.add_argument("--minimum-grid-spacing", type=float, default=1e-6, help="the minimum grid spacing in any direction. default: 1e-6")
    cls.parser.add_argument("--tecplot", action="store_true", default=False, help="output in tecplot format")

  def preprocess(self):
    self.headers = None
    self.structured_grid = StructuredGrid(self._global_data["minimum_grid_spacing"])

  def process_line(self, linenum, line):
    if linenum == 0 and self._global_data["has_headers"]:
      self.headers = line
      return

    if self._global_data["ignore_cfx_domain_lines"] and (len(line) < 3 or line == self.headers):
      return

    if len(line) < 3:
      raise ValueError("line {} has less than 3 values".format(linenum + self._global_data["lines_to_skip"]))

    self.structured_grid.append(float(line[0]), float(line[1]), float(line[2]), line[3:])

  def postprocess(self):
    if self.has_failed():
      return

    self.logger.info("finalizing structured grid")
    self.structured_grid.labels = self.headers[3:]
    self.structured_grid.finalize()
    self.logger.info("structured grid constructed with {} entries in a {}-{}-{} grid (x-y-z)".format(self.structured_grid.length(), *self.structured_grid.dimensions()))

    output_directory = os.path.dirname(self._local_data["csv_file"])

    filebasename = os.path.basename(self._local_data["csv_file"])

    if self._global_data["tecplot"]:
      output_filename = os.path.splitext(filebasename)[0] + ".dat"
      output_path = os.path.join(output_directory, output_filename)
      self.logger.info("writting to {}".format(output_path))
      with open(output_path, "w") as f:
        self.structured_grid.write_tecplot_ascii(f, filebasename)
    else:
      output_filename = "reordered-" + filebasename
      output_path = os.path.join(output_directory, output_filename)
      self.logger.info("writting to {}".format(output_path))
      with open(output_path, "w") as f:
        writer = csv.writer(f)
        if self.headers:
          writer.writerow(self.headers)
        writer.writerows(self.structured_grid.as_rows())
