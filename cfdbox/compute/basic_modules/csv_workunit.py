from __future__ import absolute_import, division

import csv
import os

from ..core.workunit import WorkUnit


class CsvWorkUnit(WorkUnit):
  DEFAULT_LINES_TO_SKIP = 0

  @classmethod
  def initialize(cls, *args, **kwargs):
    super(CsvWorkUnit, cls).initialize(*args, **kwargs)
    cls.parser.add_argument("--lines-to-skip", type=int, default=cls.DEFAULT_LINES_TO_SKIP, help="number of lines at the top of the files to skip (for headers and such). default: {}".format(cls.DEFAULT_LINES_TO_SKIP))
    cls.parser.add_argument("--csv-files", nargs="+", required=True, type=str, help="path to the csv file")

  @classmethod
  def split_input_from_args(cls, args):
    global_data = vars(args)
    local_datas = []
    for csv_file in args.csv_files:
      local_datas.append({"csv_file": csv_file})

    return global_data, local_datas

  @classmethod
  def validate_inputs(cls, global_data, local_datas):
    for l in local_datas:
      if not os.path.isfile(l["csv_file"]):
        return False, "{} is not a valid file".format(l["csv_file"])

    if global_data["lines_to_skip"] < 0:
      return False, "lines-to-skip must be a positive number"

    return True, ""

  def id(self):
    return self._local_data["csv_file"]

  def work(self):
    self.logger.info("processing {}".format(self._local_data["csv_file"]))
    try:
      self.preprocess()
      with open(self._local_data["csv_file"]) as f:
        reader = csv.reader(f)
        for i, line in self.tqdm(enumerate(reader), desc="{} lines".format(os.path.basename(self._local_data["csv_file"]))):
          if i < self._global_data["lines_to_skip"]:
            continue

          if self.process_line(i - self._global_data["lines_to_skip"], line):
            return

        self._total_line_count = i + 1 - self._global_data["lines_to_skip"]
        self.logger.info("processed {} lines".format(self._total_line_count))
    except Exception as e:
      self._failed()
      raise e
    finally:
      self.logger.info("done processing {}".format(self._local_data["csv_file"]))
      self.postprocess()

  def preprocess(self):
    pass

  def postprocess(self):
    pass

  def process_line(self, line_num, line):
    raise NotImplementedError
