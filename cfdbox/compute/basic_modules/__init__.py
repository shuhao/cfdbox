from __future__ import absolute_import

from .reorder_csv_to_structured import ReorderCsvToStructured
from .csv_averager import CsvAverager
from .grid_merger import GridMerger

workunits = [
  ReorderCsvToStructured,
  CsvAverager,
  GridMerger,
]
