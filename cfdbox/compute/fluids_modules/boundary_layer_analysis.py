from __future__ import absolute_import, division, print_function

import math
import os.path

import numpy as np
import matplotlib.pyplot as plt

from ..basic_modules.csv_workunit import CsvWorkUnit
from ..utilities.structured_grid import StructuredGrid
from ..utilities import blasius_solution


class BoundaryLayerAnalysis(CsvWorkUnit):
  description = "Calculate delta, delta^star, theta, H for a 2D, streamwise-wallnormal plane of velocity data"

  @classmethod
  def initialize(cls, *args, **kwargs):
    super(cls, cls).initialize(*args, **kwargs)
    cls.parser.add_argument(
      "--velocity-u-index", type=int, required=True,
      help="the column index of velocity u, starts at 0"
    )

    cls.parser.add_argument(
      "--nu", type=float, required=True,
      help="kinematic viscosity in SI units"
    )

    cls.parser.add_argument(
      "--u0", type=float, required=True,
      help="free stream velocity in SI units"
    )

    cls.parser.add_argument(
      "--no-slip-starts", type=float,
      nargs="?", default=0.0,
      help="the streamwise coordinate (x) where the no slip wall starts in the simulation in SI units. default: 0"
    )

    cls.parser.add_argument(
      "--z-plane", type=float, nargs="?", default=0.5,
      help="z plane percentage to take perform the boundary layer analysis. default: 0.5 for the middle of the domain"
    )

    cls.parser.add_argument(
      "--show-velocity-profiles-at-x", type=float,
      nargs="*", default=[],
      help="a list of streamwise coordinates where the velocity profiles are supposed to be shown",
    )

    cls.parser.add_argument(
      "--x-marker-lines", type=float,
      nargs="*", default=[],
      help="x coordinates to put a vertical line as a marker on the boundary layer profile plot"
    )

    cls.parser.add_argument(
      "--x-filter", type=float,
      nargs="*", default=[],
      help="format: x_low x_high, this way the program will not plot below x_low and will not plot above x_high"
    )

    cls.parser.add_argument(
      "--minimum-grid-spacing", type=float,
      nargs="?", default=1e-6,
      help="the minimum grid spacing in any direction. default: 1e-6"
    )

    cls.parser.add_argument(
      "--output-thickness-path", nargs="?", default=None,
      help="path where the thickness will be outputted"
    )

    cls.parser.add_argument("--do-not-plot", action="store_true")

  @classmethod
  def before_any_work_runs(cls, global_data, local_datas):
    cls.logger.info("read {} lines of blasius solution from {}".format(len(blasius_solution.RAW_BLASIUS_SOLUTION), blasius_solution.BLASIUS_DATA_PATH))

  @classmethod
  def plot_thickness(cls, input_data, output, ttype, name):
    # Duplicate work, but we can figure out optimize later
    invalid_x_coords = [x for i, x in enumerate(output["x_coords"]) if output["invalid_mask"][i]]
    invalid_points = [t for i, t in enumerate(output[ttype]) if output["invalid_mask"][i]]

    plt.figure()
    plt.plot(output["x_coords"], output["theoretical_{}".format(ttype)], "k--")
    plt.plot(output["x_coords"], output[ttype], "k-")

    if len(invalid_x_coords) > 0:
      cls.logger.error("invalid x coordinates found: {}".format(invalid_x_coords))
      plt.plot(invalid_x_coords, invalid_points, "rx", s=20)

    cls.plot_marker_line(input_data)

    plt.legend(["Blasius", "Measured"], loc="upper left")
    title = "{} for {}".format(ttype, name)
    plt.title(title)

  @classmethod
  def plot_marker_line(cls, input_data):
    limits = plt.axis()
    ys = [limits[2], limits[3]]
    for x in input_data["global"]["x_marker_lines"]:
      plt.plot([x, x], ys, "b-")

    for x in input_data["global"]["show_velocity_profiles_at_x"]:
      plt.plot([x, x], ys, "r--")

  @classmethod
  def output_thickenss(cls, path_prefix, output, ttype):
    path = path_prefix + "." + ttype + ".csv"
    with open(path, "w") as f:
      print("x,{}".format(ttype), file=f)
      for i, x in enumerate(output["x_coords"]):
        print("{},{}".format(x, output[ttype][i]), file=f)

  @classmethod
  def all_work_done(cls, data):
    """An after all work is done synchronization point.

    data: unit.id(), unit.input(), unit.output()
    """
    for work_id, input_data, output in data:
      name = os.path.splitext(os.path.basename(work_id))[0]

      if not input_data["global"]["do_not_plot"]:
        cls.plot_thickness(input_data, output, "bl_thicknesses", name)
        cls.plot_thickness(input_data, output, "disp_thicknesses", name)
        cls.plot_thickness(input_data, output, "momentum_thicknesses", name)
        cls.plot_thickness(input_data, output, "vorticity_thicknesses", name)

      if input_data["global"]["output_thickness_path"]:
        cls.output_thickenss(input_data["global"]["output_thickness_path"], output, "bl_thicknesses")
        cls.output_thickenss(input_data["global"]["output_thickness_path"], output, "disp_thicknesses")
        cls.output_thickenss(input_data["global"]["output_thickness_path"], output, "momentum_thicknesses")
        cls.output_thickenss(input_data["global"]["output_thickness_path"], output, "vorticity_thicknesses")

      # Shape Factor
      if not input_data["global"]["do_not_plot"]:
        plt.figure()
        plt.plot([min(output["x_coords"]), max(output["x_coords"])], [2.59, 2.59], "k--")
        plt.plot(output["x_coords"], output["shape_factor"], "k-")
        limits = list(plt.axis())
        limits[2] = 1
        limits[3] = 3
        plt.axis(limits)

        cls.plot_marker_line(input_data)

        plt.legend(["Blasius", "Measured"], loc="lower left")
        plt.title("Shape Factor for {}".format(name))

      # TODO: show the actual coordinate rather than the one specified
      for n, ys in enumerate(output["ys_at_specified_x"]):
        x = output["x_coords"][n]
        if ys is None:
          cls.logger.warn("specified x {} is an invalid point".format(x))

        ys = np.array(ys) - ys[0]
        velocity_profile = output["velocity_profiles_at_specified_x"][n]
        i = output["velocity_profile_x_indexes"][n]

        if not input_data["global"]["do_not_plot"]:
          blasius_ys, blasius_profile = blasius_solution.velocity_profile_at(x=x, u0=input_data["global"]["u0"], nu=input_data["global"]["nu"])

          plt.figure()
          plt.plot(blasius_profile, blasius_ys, "k--")
          plt.plot(velocity_profile, ys, "k-")
          limits = plt.axis()
          xlimits = [limits[0], limits[1]]
          plt.legend(["Blasius", "Measured"])

          bl_thickness = output["bl_thicknesses"][i]
          momentum_thickness = output["momentum_thicknesses"][i]
          disp_thickness = output["disp_thicknesses"][i]
          vort_thickness = output["vorticity_thicknesses"][i]

          plt.plot(xlimits, [bl_thickness, bl_thickness], "r--")
          for_name = "{} for {}".format(input_data["global"]["show_velocity_profiles_at_x"][n], name)
          plt.title("Velocity profile at {}".format(for_name))

        friction_velocity = input_data["global"]["nu"] * (velocity_profile[1] - velocity_profile[0]) / (ys[1] - ys[0])

        cls.logger.info("bl_thickness({}) = {}".format(for_name, bl_thickness))
        cls.logger.info("momentum_thickness({}) = {}".format(for_name, momentum_thickness))
        cls.logger.info("disp_thickness({}) = {}".format(for_name, disp_thickness))
        cls.logger.info("vort_thickness({}) = {}".format(for_name, vort_thickness))
        cls.logger.info("friction_velocity({}) = {} rho".format(for_name, friction_velocity))

    if not input_data["global"]["do_not_plot"]:
      plt.show()

  def preprocess(self):
    self.structured_grid = StructuredGrid(self._global_data["minimum_grid_spacing"])
    self.bl_velocity_required = self._global_data["u0"] * 0.99

  def re(self, x):
    return self._global_data["u0"] * x / self._global_data["nu"]

  def process_line(self, linenum, line):
    self.structured_grid.append(float(line[0]), float(line[1]), float(line[2]), line[3:])

  def postprocess(self):
    self.logger.info("finalizing grid")
    self.structured_grid.finalize()

    # Get our parameters!
    lx, ly, lz = self.structured_grid.dimensions()

    velocity_profile_x_indexes = self.structured_grid.find_closest_coordinate_indexes(
      self._global_data["show_velocity_profiles_at_x"],
      StructuredGrid.DIRECTION_X
    )

    z_plane_coordinate = self._global_data["z_plane"] * self.structured_grid.all_z[0] + self._global_data["z_plane"] * self.structured_grid.all_z[-1]
    k = self.structured_grid.find_closest_coordinate_indexes(
      [z_plane_coordinate],
      StructuredGrid.DIRECTION_Z
    )[0]

    # Get the velocity profile!
    velocity_profiles = [[] for _ in range(lx)]
    streamwise_ys = [[] for _ in range(lx)]
    invalid_mask = []

    u_index = self._global_data["velocity_u_index"]

    for i in range(lx):
      max_u = 0
      for j in range(ly):
        data = self.structured_grid[i, j, k]

        velocity_u = float(data[u_index])
        velocity_profiles[i].append(velocity_u)
        streamwise_ys[i].append(data[1])

        # Detect invalid stuff
        if velocity_u > max_u:
          max_u = velocity_u

      invalid_mask.append(max_u < self.bl_velocity_required)
      if invalid_mask[-1]:
        self.logger.warn("invalid point: {}".format(data[0]))
        for _c, vi in enumerate(velocity_profile_x_indexes):
          if vi > i:
            velocity_profile_x_indexes[_c] -= 1
          elif vi == i:
            velocity_profile_x_indexes[_c] = None

    # Compute the integral parameter!

    x_coords = []
    bl_thicknesses = []
    disp_thicknesses = []
    momentum_thicknesses = []
    vorticity_thicknesses = []

    theoretical_bl_thicknesses = []
    theoretical_disp_thicknesses = []
    theoretical_momentum_thicknesses = []
    theoretical_vorticity_thicknesses = []

    for i, velocity_profile in enumerate(velocity_profiles):
      if invalid_mask[i]:
        continue

      x = self.structured_grid[i, 0, k][0]

      if len(self._global_data["x_filter"]) == 2:
        if x < self._global_data["x_filter"][0]:
          continue
        elif x > self._global_data["x_filter"][1]:
          break

      bl_thickness = self.compute_bl_thickness(velocity_profile, streamwise_ys[i])
      disp_thickness = self.compute_displacement_thickness(velocity_profile, streamwise_ys[i])
      momentum_thickness = self.compute_momentum_thickness(velocity_profile, streamwise_ys[i])
      vort_thickness = self.compute_vorticity_thickness(velocity_profile, streamwise_ys[i])

      x_coords.append(x)
      bl_thicknesses.append(bl_thickness)
      disp_thicknesses.append(disp_thickness)
      momentum_thicknesses.append(momentum_thickness)
      vorticity_thicknesses.append(vort_thickness)

      x_since_no_slip = x - self._global_data["no_slip_starts"]
      sqrt_re = math.sqrt(self.re(x_since_no_slip))
      theoretical_bl_thicknesses.append(4.91 * x_since_no_slip / sqrt_re)
      theoretical_disp_thicknesses.append(1.721 * x_since_no_slip / sqrt_re)
      theoretical_momentum_thicknesses.append(0.665 * x_since_no_slip / sqrt_re)
      theoretical_vorticity_thicknesses.append(0)

    self._output = {
      "velocity_profile_x_indexes": velocity_profile_x_indexes,
      "ys_at_specified_x": [streamwise_ys[i] if i is not None else None for i in velocity_profile_x_indexes if i is not None],
      "velocity_profiles_at_specified_x": [velocity_profiles[i] if i is not None else None for i in velocity_profile_x_indexes],
      "x_coords": x_coords,
      "bl_thicknesses": bl_thicknesses,
      "disp_thicknesses": disp_thicknesses,
      "momentum_thicknesses": momentum_thicknesses,
      "vorticity_thicknesses": vorticity_thicknesses,
      "shape_factor": np.array(disp_thicknesses) / np.array(momentum_thicknesses),
      "invalid_mask": invalid_mask,
      "theoretical_bl_thicknesses": theoretical_bl_thicknesses,
      "theoretical_disp_thicknesses": theoretical_disp_thicknesses,
      "theoretical_momentum_thicknesses": theoretical_momentum_thicknesses,
      "theoretical_vorticity_thicknesses": theoretical_vorticity_thicknesses,
    }

  def compute_bl_thickness(self, velocity_profile, ys):
    for j, u in enumerate(velocity_profile):
      if u > self.bl_velocity_required:
        if j == 0:
          return 0

        prev_u = velocity_profile[j - 1]
        current_y = ys[j]
        prev_y = ys[j - 1]

        return (self.bl_velocity_required - prev_u) * (current_y - prev_y) / (u - prev_u) + prev_y - ys[0]

  def compute_displacement_thickness(self, velocity_profile, ys):
    thickness = 0

    for j, u in enumerate(velocity_profile):
      if j == 0:
        continue

      prev_u = velocity_profile[j - 1]
      dy = ys[j] - ys[j - 1]
      thickness += dy * (self._disp_thickness_step(u) + self._disp_thickness_step(prev_u)) / 2

    return thickness

  def compute_momentum_thickness(self, velocity_profile, ys):
    thickness = 0

    for j, u in enumerate(velocity_profile):
      if j == 0:
        continue

      prev_u = velocity_profile[j - 1]
      dy = ys[j] - ys[j - 1]
      thickness += dy * (self._momentum_thickness_step(u) + self._momentum_thickness_step(prev_u)) / 2

    return thickness

  def compute_vorticity_thickness(self, velocity_profile, ys):
    dudy = self.diff_central(ys, velocity_profile, exclude_first_last=True)
    return self._global_data["u0"] / np.max(dudy)

  def diff_central(self, x, y, exclude_first_last=False):
    x = np.array(x)
    y = np.array(y)
    x0 = x[:-2]
    x1 = x[1:-1]
    x2 = x[2:]
    y0 = y[:-2]
    y1 = y[1:-1]
    y2 = y[2:]
    f = (x2 - x1) / (x2 - x0)
    center_differences = (1 - f) * (y2 - y1) / (x2 - x1) + f * (y1 - y0) / (x1 - x0)
    if exclude_first_last:
      return center_differences

    first_difference = np.array([(y[1] - y[0]) / (x[1] - x[0])])
    last_difference = np.array([(y[-1] - y[-2]) / (x[-1] - x[-2])])
    return np.concatenate([first_difference, center_differences, last_difference])

  def _disp_thickness_step(self, uy):
    return 1 - uy / self._global_data["u0"]

  def _momentum_thickness_step(self, uy):
    return (uy / self._global_data["u0"]) * (1 - uy / self._global_data["u0"])
