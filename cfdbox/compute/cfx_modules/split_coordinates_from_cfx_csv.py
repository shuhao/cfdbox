from __future__ import absolute_import

import csv
import os.path

from ..basic_modules.csv_workunit import CsvWorkUnit


class SplitCoordinatesFromCfxCsv(CsvWorkUnit):
  description = "split the xyz coordinates from a CFX csv file into two files: a coordinates file and a data file"
  DEFAULT_LINES_TO_SKIP = 0

  @classmethod
  def initialize(cls, *args, **kwargs):
    super(SplitCoordinatesFromCfxCsv, cls).initialize(*args, **kwargs)
    cls.parser.add_argument("--output-directory", nargs="?", type=str, help="the output directory for all the files")

  @classmethod
  def validate_inputs(cls, global_data, local_datas):
    if not os.path.isdir(global_data["output_directory"]):
      return False, "{} is not a valid directory".format(global_data["output_directory"])

    return super(SplitCoordinatesFromCfxCsv, cls).validate_inputs(global_data, local_datas)

  def preprocess(self):
    filename = os.path.basename(self._local_data["csv_file"])
    filename = os.path.splitext(filename)[0]

    coordinates_filename = filename + "-coordinates.csv"
    self._output["coordinates_path"] = os.path.join(self._global_data["output_directory"], coordinates_filename)
    self.coordinates_file = open(self._output["coordinates_path"], "w")
    self.coordinates_writer = csv.writer(self.coordinates_file)

    data_filename = filename + "-data.csv"
    self._output["data_path"] = os.path.join(self._global_data["output_directory"], data_filename)
    self.data_file = open(self._output["data_path"], "w")
    self.data_writer = csv.writer(self.data_file)

    for i in range(self._global_data["lines_to_skip"]):
      self.coordinates_writer.writerow([])
      self.data_writer.writerow([])

  def postprocess(self):
    self.coordinates_file.close()
    self.data_file.close()
    self._done()

  def process_line(self, line_num, line):
    self.coordinates_writer.writerow(line[:3])
    self.data_writer.writerow(line[3:])
