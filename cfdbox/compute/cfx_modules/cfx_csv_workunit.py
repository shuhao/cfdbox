from __future__ import absolute_import

import csv
import os.path

from ..basic_modules.csv_workunit import CsvWorkUnit
from ..utilities import underscore
from ...common.utils import is_number


class CfxCsvWorkUnit(CsvWorkUnit):
  DEFAULT_LINES_TO_SKIP = 0

  @staticmethod
  def is_line_data_line(line):
    return len(line) > 0 and all(map(is_number, line))

  @classmethod
  def initialize(cls, *args, **kwargs):
    super(CfxCsvWorkUnit, cls).initialize(*args, **kwargs)
    cls.parser.add_argument("--coordinates-file", type=str, default=None, help="path to the csv file with only the xyz coordinates in the same row order as the data csv files. the x, y, z coordinate must be the same column and the first row after lines to skip must be the labels rather than the coordinates. if none is specified, it is assumed the coordinates are in every csv file.")
    cls.parser.add_argument("--output-directory", nargs="?", type=str, help="the output directory for all the files")
    cls.parser.add_argument("--label-line-num", type=int, default=5, help="the line number of the labels (the first line is 0). default: 5")

  @classmethod
  def split_input_from_args(cls, args):
    global_data, local_datas = super(CfxCsvWorkUnit, cls).split_input_from_args(args)

    if global_data["coordinates_file"]:
      if not os.path.isfile(global_data["coordinates_file"]):
        global_data["coordinates_file_not_found"] = True
        return global_data, local_datas

      coordinates = []
      cls.logger.info("acquiring coordinates from coordinates file")
      with open(global_data["coordinates_file"]) as f:
        reader = csv.reader(f)
        for i, line in cls.tqdm(enumerate(reader), desc="coordinates count"):

          if i < args.lines_to_skip:
            continue

          # We want to preserve the header
          try:
            line = tuple(map(float, line))
          except ValueError:
            line = tuple(line)

          coordinates.append(line)

      global_data["coordinates"] = coordinates

    return global_data, local_datas

  @classmethod
  def validate_inputs(cls, global_data, local_datas):
    if "coordinates_file_not_found" in global_data:
      return False, "{} is not a valid file".format(global_data["coordinates_file"])

    if not os.path.isdir(global_data["output_directory"]):
      return False, "{} is not a valid directory".format(global_data["output_directory"])

    return super(CfxCsvWorkUnit, cls).validate_inputs(global_data, local_datas)

  def set_labels(self, labels):
    self.labels = labels

  def process_line(self, line_num, line):
    if line_num == self._global_data["label_line_num"]:
      self.set_labels([underscore(item.split("[")[0].strip()) for item in line])
      return None, None

    if not CfxCsvWorkUnit.is_line_data_line(line):
      self.logger.warn("line {} does not have data, skipping".format(line_num))
      return None, None

    if "coordinates" not in self._global_data:
      return tuple(map(float, line[:3])), line[3:]

    if line_num >= len(self._global_data["coordinates"]):
      coordinates = []
    else:
      coordinates = self._global_data["coordinates"][line_num]

    if len(coordinates) != 3:
      raise RuntimeError("line {} does not have corresponding coordinates (coordinates: {} line: {})".format(line_num, coordinates, line))

    return coordinates, line
