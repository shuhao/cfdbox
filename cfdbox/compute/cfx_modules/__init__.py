from __future__ import absolute_import

from .split_coordinates_from_cfx_csv import SplitCoordinatesFromCfxCsv

workunits = [
  SplitCoordinatesFromCfxCsv,
]
