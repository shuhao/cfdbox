from __future__ import division

from builtins import range

# This module is based on:
# http://visitusers.org/index.php?title=Writing_Tecplot_Using_Python

FIELD_WIDTH = 10

# def pad_and_truncate(s, max_width):
#   s2 = StringIO()
#   s2.write(" ")
#   num_of_spaces = -(len(s) - max_width + 1)  # Always have a " " in front so max_width = max_width - 1
#   if num_of_spaces >= 0:
#     # positive case, we add padding
#     s2.write(" " * num_of_spaces)
#     s2.write(s)
#   else:
#     # negative case, we remove some strings
#     s2.write(s[:num_of_spaces])
#   return s2.getvalue()


def pad_and_truncate(s, width):
  s2 = str(s)
  while len(s2) < width:
    s2 = ' ' + s2
  if s2[0] != ' ':
    s2 = ' ' + s2
  if len(s2) > width:
    s2 = s2[:width]
  return s2


_DIMENSION_NAMES = ["I", "J", "K"]


class RectlinearData(object):
  def __init__(self, labels):
    self.data = {}
    self.labels = labels

    self.node_counts = None

  def get_node_counts(self):
    if self.node_counts:
      return self.node_counts

    unique_coords = [set() for i in range(self.__class__.DIMENSIONS)]
    for coord in self.data:
      for i, c in enumerate(coord):
        unique_coords[i].add(c)

    self.node_counts = tuple(map(len, unique_coords))
    return self.node_counts

  def write_tecplot_ascii(self, f):
    f.write('Variables="X","Y"')
    if self.__class__.DIMENSIONS > 2:
      f.write(',"Z"')

    for label in self.labels:
      f.write(',"{}"'.format(label))

    f.write("\n\n")
    counts = self.get_node_counts()
    f.write("Zone ")

    zones = []
    for i, count in enumerate(counts):
      zones.append("{}={}".format(_DIMENSION_NAMES[i], pad_and_truncate(count, 6)))

    f.write(",".join(zones))

    f.write(', F=POINT\n')

    ordered_data = [(coordinate, self.data[coordinate]) for coordinate in self.data]
    ordered_data.sort(key=lambda x: list(reversed(x[0])))

    for coordinate, values in ordered_data:
      for c in coordinate:
        f.write(pad_and_truncate(c, FIELD_WIDTH))

      for value in values:
        f.write(pad_and_truncate(value, FIELD_WIDTH))

      f.write("\n")

    return f

  def get_point(self, *coordinate):
    return self.data.get(coordinate, None)

  def set_point(self, *args):
    coord = args[:self.DIMENSIONS]
    values = args[self.DIMENSIONS:]
    self.data[coord] = values


class RectlinearData2D(RectlinearData):
  DIMENSIONS = 2


class RectlinearData3D(RectlinearData):
  DIMENSIONS = 3
