from __future__ import absolute_import, division, print_function

import logging


_DIMENSION_NAMES = ["I", "J", "K"]
FIELD_WIDTH = 10


class StructuredGrid(object):
  DIMENSIONS = 3

  DIRECTION_X = 0
  DIRECTION_Y = 1
  DIRECTION_Z = 2

  def __init__(self, minimum_grid_spacing):
    self.logger = logging.getLogger("sg")
    self.logger.setLevel(logging.DEBUG)
    self._minimum_grid_spacing = minimum_grid_spacing

    self.grid = []
    self.labels = None
    self.all_x, self.all_y, self.all_z = set(), set(), set()

  def append(self, x, y, z, line):
    line = ((x, y, z), line)
    self.grid.append(line)
    return self

  def dimensions(self):
    return len(self.all_x), len(self.all_y), len(self.all_z)

  def length(self):
    return len(self.grid)

  def finalize(self):
    # A possible case where we run into failure is let's say two coordinates
    # of the values of 1.85495019e-1 and 1.85494989e-1. If the rounding digits
    # is 5, it will round to either 1.8549e-1 or 1.8550e-1.
    #
    # To solve this problem, we gather all the unique x, y, and z coordinates,
    # sort them, and then compare the difference between two adjacent
    # coordinates. If the adjacent coordinates is below the minimum_grid_spacing,
    # we mark these coordinates as the same by mapping the smaller coordinate
    # as the larger coordinate such that when we output, if it encounters a
    # smaller coordinate, it will just use the value of the larger coordinate.
    #
    # NOTE! JUST BECAUSE THE SORTING FUNCTION USE THE LARGER VALUE DOES NOT
    # MEAN THE OUTPUT WILL. THE OUTPUT WILL PRESERVE THE ORIGINAL NUMERICAL
    # ERROR TO AVOID CONFUSION.
    #
    # TODO: numerically verify this is a valid solution to solve this problem
    # down to the double accuracy.

    # So we sort!
    # TODO: if the grid we get is already structured, we can skip this part
    # and go straight to the second part.

    self.logger.info("getting all coordinates")
    for coord, _ in self.grid:
      self.all_x.add(coord[0])
      self.all_y.add(coord[1])
      self.all_z.add(coord[2])

    self.all_x = list(self.all_x)
    self.all_y = list(self.all_y)
    self.all_z = list(self.all_z)

    self.all_x.sort()
    self.all_y.sort()
    self.all_z.sort()

    x_map = self._determine_coordinate_mapping(self.all_x)
    y_map = self._determine_coordinate_mapping(self.all_y)
    z_map = self._determine_coordinate_mapping(self.all_z)

    self.logger.info("sorting all coordinates")
    self.grid.sort(key=self._sort_order_function(x_map, y_map, z_map))

    # Now we determine the dimensions of the structured grid
    # Also filter out identical points
    self.logger.info("filtering identical points")
    self.all_x, self.all_y, self.all_z = [], [], []
    last_x, last_y, last_z = float("inf"), float("inf"), float("inf")
    # It will be incremented immediately because last_x = inf
    x_done = -1
    y_done = -1

    # For identical point detection...
    filtered_grid = []

    for coord, dataline in self.grid:
      current_x, current_y, current_z = coord

      if not (abs(current_x - last_x) <= self._minimum_grid_spacing and abs(current_y - last_y) <= self._minimum_grid_spacing and abs(current_z - last_z) < self._minimum_grid_spacing):
        filtered_grid.append((coord, dataline))

      last_x, last_y, last_z = current_x, current_y, current_z

    self.grid = filtered_grid

    # Let's find out the dimensions
    self.logger.info("determining grid dimensions")
    for coord, dataline in self.grid:
      current_x, current_y, current_z = coord

      if current_y < last_y:
        self.all_z.append(current_z)
        y_done += 1

      if y_done <= 0 and current_x < last_x:
        self.all_y.append(current_y)
        x_done += 1

      if x_done <= 0:
        self.all_x.append(current_x)

      last_x, last_y, last_z = current_x, current_y, current_z

    return self

  def as_rows(self):
    return map(self._flatten_row, self.grid)

  def _flatten_row(self, row):
    return list(row[0]) + list(row[1])

  def find_closest_coordinate_indexes(self, coordinates, direction):
    current_minimum_indexes = [0 for _ in coordinates]
    current_minimum = [float("inf") for _ in coordinates]

    for i in range(self.dimensions()[direction]):
      accessor = [0, 0, 0]
      accessor[direction] = i
      current_coordinate = self[accessor][direction]

      for j, c in enumerate(coordinates):
        dist = abs(current_coordinate - c)
        if dist < current_minimum[j]:
          current_minimum[j] = dist
          current_minimum_indexes[j] = i

    return current_minimum_indexes

  def __getitem__(self, tup):
    i, j, k = tup
    lx, ly, lz = self.dimensions()
    unrolled_index = k * (lx * ly) + j * lx + i
    return self._flatten_row(self.grid[unrolled_index])

  # Based on # http://visitusers.org/index.php?title=Writing_Tecplot_Using_Python
  def write_tecplot_ascii(self, f, title):
    f.write('Variables="X","Y"')
    if self.__class__.DIMENSIONS > 2:
      f.write(',"Z"')

    for label in self.labels:
      f.write(',"{}"'.format(label))

    f.write("\n\n")
    counts = self.dimensions()
    f.write('Zone T="{}", '.format(title))

    zones = []
    for i, count in enumerate(counts):
      zones.append("{}={}".format(_DIMENSION_NAMES[i], self._pad_and_truncate(count, 6)))

    f.write(",".join(zones))

    f.write(', F=POINT\n')

    for coordinate, values in self.grid:
      for c in coordinate:
        f.write(self._pad_and_truncate(c, FIELD_WIDTH))

      for value in values:
        value = float(value)
        f.write(self._pad_and_truncate(value, FIELD_WIDTH))

      f.write("\n")

    return f

  def _pad_and_truncate(self, s, width):
    # Must have a " " preceding
    # Must be len(s) = width
    # Doesn't quite work if floats/ints gets very long
    if isinstance(s, int):
      formatstr = "{:>%s}" % (width - 1)
    else:
      formatstr = "{:>%s.%s}" % (width - 1, width - 1)

    return " " + formatstr.format(s)

  def _sort_order_function(self, x_map, y_map, z_map):
    def sort_func(line):
      x, y, z = line[0]
      return z_map.get(z, z), y_map.get(y, y), x_map.get(x, x)

    return sort_func

  def _determine_coordinate_mapping(self, sorted_coordinates):
    coordinate_mapping = {}

    for i, c in enumerate(sorted_coordinates):
      if i == 0:
        continue

      previous_c = sorted_coordinates[i - 1]

      if abs(c - previous_c) <= self._minimum_grid_spacing:
        coordinate_mapping[previous_c] = c

    return coordinate_mapping
