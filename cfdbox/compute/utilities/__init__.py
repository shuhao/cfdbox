import re

# From https://gist.github.com/jaytaylor/3660565
_underscorer1 = re.compile(r'(.)([A-Z][a-z]+)')
_underscorer2 = re.compile(r'([a-z0-9])([A-Z])')


def underscore(s):
  subbed = _underscorer1.sub(r'\1_\2', s)
  return _underscorer2.sub(r'\1_\2', subbed).lower()
