from __future__ import absolute_import, division, print_function

import argparse
import logging
import os
import sys

from .runners.local_multi import LocalMultiProcessRunner
from ...common.tqdm import tqdm


# TODO:
# Potential interfaces:
# 1. Commandline single script ($ python my_work_unit.py) [DONE-ish]
# 2. Commandline toolbox ($ cfdbox-compute builtin_workunit_1)
# 3. Webbased caller (interface to select arguments, save run history and results)
#
# Potential application:
# 1. Local machine serial
# 2. Local machine multiprocessing
# 3. Local machine multiprocessing within a larger application
# 4. Multiple machine
# 5. Multiple machine within a larger application
#
# Map Reduce?


_default = object()


class WorkUnit(object):
  @classmethod
  def main(cls, runner_cls=LocalMultiProcessRunner, args=None):
    """Main entry point if this is running as a standalone."""
    logging.basicConfig(format="[%(asctime)s][%(name)s][%(levelname).1s] %(message)s", datefmt="%Y-%m-%d %H:%M:%S")

    cls.initialize()
    args = cls.parser.parse_args(args=args)
    return cls.run(runner_cls, args)

  @classmethod
  def run(cls, runner_cls=LocalMultiProcessRunner, args=None):
    global_data, local_data = cls.split_input_from_args(args)
    valid, reason = cls.validate_inputs(global_data, local_data)
    if not valid:
      cls.logger.error(reason)
      sys.exit(1)

    cls.before_any_work_runs(global_data, local_data)

    runner = runner_cls({"parallel": args.parallel})
    data = runner.run(cls, global_data, local_data)

    return cls.all_work_done(data)

  @classmethod
  def initialize(cls, parser=None, loglevel=_default):
    cls.parser = parser or argparse.ArgumentParser(description=cls.description)
    # only ever called for single machine run
    cls.parser.add_argument("--parallel", type=int, default=1, help="the number of workers in parallel")
    cls.logger = logging.getLogger(cls.__name__)
    if loglevel is _default:
      loglevel = getattr(logging, os.environ.get("LOG_LVL", "INFO"), logging.INFO)

    cls.logger.setLevel(loglevel)

  @classmethod
  def split_input_from_args(cls, args):
    """Returns a global data hash and a list of local data hash
    """
    return {}, []

  @classmethod
  def validate_inputs(cls, global_data, local_datas):
    return True, ""

  @classmethod
  def before_any_work_runs(cls, global_data, local_datas):
    pass

  @classmethod
  def tqdm(cls, iter, **kwargs):
    kwargs["logger"] = cls.logger
    return tqdm(iter, **kwargs)

  @classmethod
  def all_work_done(cls, data):
    """An after all work is done synchronization point.

    data: unit.id(), unit.input(), unit.output()
    """
    return data

  def __init__(self, global_data, local_data):
    self._global_data = global_data
    self._local_data = local_data
    self._output = {}
    self._done_state = False
    self._failed_state = False

  def get_input(self):
    return self._global_data, self._local_data

  def id(self):
    return self._local_data

  def work(self):
    """Runs and returns whether or not the run is successful"""
    self._done()

  def _done(self):
    self._done_state = True

  def _failed(self):
    self._failed_state = True

  def is_done(self):
    return self._done_state

  def has_failed(self):
    return self._failed_state

  def input(self):
    return {"local": self._local_data, "global": self._global_data}

  def output(self):
    """Returns a JSON serializable object that represents the output of this run"""
    return self._output
