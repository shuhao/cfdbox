class LocalSerialRunner(object):
  def __init__(self, config):
    self.config = config

  def run(self, workunit_cls, global_data, local_data):
    def _run_one(data):
      unit = workunit_cls(global_data, data)
      unit.work()
      return unit.id(), unit.input(), unit.output()

    return list(map(_run_one, local_data))
