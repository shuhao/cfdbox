from __future__ import absolute_import

import multiprocessing
import logging
import time


# We need this because we cannot pickle a local function...
class ActualRunTask(object):
  def __init__(self, workunit_cls, global_data):
    self.workunit_cls = workunit_cls
    self.global_data = global_data

  def __call__(self, local_data):
    unit = self.workunit_cls(self.global_data, local_data)
    unit.work()
    return unit.id(), unit.input(), unit.output()


class LocalMultiProcessRunner(object):
  def __init__(self, config):
    self.logger = logging.getLogger("lmpr")
    self.logger.setLevel(logging.DEBUG)
    self.parallel = config["parallel"]

    self.pool = multiprocessing.Pool(processes=self.parallel)

  def run(self, workunit_cls, global_data, local_datas):
    start = time.time()
    self.logger.info("starting processing with {} workers".format(self.parallel))
    if getattr(workunit_cls, "SERIAL_ONLY", False):
      raise RuntimeError("LocalMultiProcessRunner cannot run SERIAL_ONLY jobs")

    data = self.pool.map(ActualRunTask(workunit_cls, global_data), local_datas)
    delta = time.time() - start
    self.logger.info("done! took {}s...".format(delta))
    return data
