from __future__ import absolute_import, print_function

import argparse
import logging
import os
import sys

from . import basic_modules
from . import cfx_modules
from . import fluids_modules
from .utilities import underscore
from .core.runners.local_multi import LocalMultiProcessRunner
from .core.runners.local_serial import LocalSerialRunner

workunits = []
workunits.extend(basic_modules.workunits)
workunits.extend(cfx_modules.workunits)
workunits.extend(fluids_modules.workunits)


def main(runner_cls=LocalMultiProcessRunner):
  logging.basicConfig(format="[%(asctime)s][%(name)s][%(levelname).1s] %(message)s", datefmt="%Y-%m-%d %H:%M:%S")
  parser = argparse.ArgumentParser(description="{0} multicall executable".format(os.path.basename(sys.argv[0])))
  subparsers = parser.add_subparsers()
  for wu in workunits:
    name = underscore(wu.__name__)
    subparser = subparsers.add_parser(name, help=wu.description)
    wu.initialize(parser=subparser)
    subparser.set_defaults(workunit=wu)

  args = parser.parse_args()

  if len(vars(args)) == 0:
    # If you call this with no arguments, it will break like this:
    # https://bugs.python.org/issue16308
    parser.print_usage()
    print("{}: error: too few arguments".format(parser.prog), file=sys.stderr)
    sys.exit(1)

  workunit = args.workunit
  delattr(args, "workunit")  # because we need to serialize in multiprocessing

  if os.environ.get("SERIAL") == "1":
    runner_cls = LocalSerialRunner

  return workunit.run(args=args, runner_cls=runner_cls)
